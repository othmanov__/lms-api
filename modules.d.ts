
declare namespace NodeJS {
    export interface ProcessEnv {
        FROM_ADDRESS:string
        SMTP_HOST:string
        SMTP_USER:string
        SMTP_PORT:string
        SMTP_SECRET:string
        HOSTNAME:string
        PORT:string
        MEDIA_BASE:string
        PASSWORD_SALT:string
        DB_URL:string
        DB_NAME:string
        DB_USER:string
        DB_PASSWORD:string
        LOG_WARNINGS:boolean
        LOG_ERRORS:boolean
        LOG_INFO:boolean
        LOGS_LOCATION:string
        LOGS:boolean
        EMAILS:boolean
        NODE_ENV:'test':'dev':'prod'
        MOCKS_DIR:string
    }
  }