import { RessourceTypeEnum } from "@app/entity/enums";

export const acceptedUploadExtensions = [
  RessourceTypeEnum.CSV,
  RessourceTypeEnum.DEFAULT,
  RessourceTypeEnum.DOCX,
  RessourceTypeEnum.JPG,
  RessourceTypeEnum.MD,
  RessourceTypeEnum.MP3,
  RessourceTypeEnum.ODT,
  RessourceTypeEnum.OGG,
  RessourceTypeEnum.PDF,
  RessourceTypeEnum.PNG,
  RessourceTypeEnum.PPTX,
  RessourceTypeEnum.XLS
];
export const maxFileUploads = 10;
export const maxFileUploadSize = 8;
export const uploadTimeout = 30000; // 30 s
