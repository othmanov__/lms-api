import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository, getManager } from "typeorm";
import environment from "@env";
import { closeConnection, openConnection } from "@app/index";
import { Ressource } from "@app/entity/Ressource";
const host = environment.api;
const should = chai.should();
import { expect } from "chai";
import { clearMediaFolder } from "../../utils";
import {
  badRessourceMocks,
  ressourceMocksTypes,
  ressourceMocks,
  createMockRessource
} from "tests/units.utils";
let connection: Connection = null;

describe("Ressource model", async function() {
  before(async () => {
    connection = await getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    connection.manager.delete(Ressource, {});
  });
  afterEach(async () => {
    connection.manager.delete(Ressource, {});
  });
  after(async () => {
    await connection.getRepository(Ressource).delete({});
    await closeConnection();
    clearMediaFolder();
  });

  describe("Bad ressources types must fail ", () => {
    badRessourceMocks.forEach(bad => {
      it(`Should fail saving a bad Ressource type ${bad.type}`, async () => {
        try {
          await createMockRessource(bad);
        } catch (error) {
          chai.expect(error).to.be.ok;
        }
      });
    });
  });
  describe("Good ressources type should succed", () => {
    context("Quering array of ressources", () => {
      it(`Should query ressource of types ${ressourceMocksTypes.join()}`, async () => {
        const elements = await getRepository(Ressource).create(ressourceMocks);
        await getManager().save(elements);
        const created = await getRepository(Ressource).find();
        expect(created).to.be.lengthOf(elements.length);
        expect(created).to.deep.equal(elements);
      });
      after(async () => {
        await getRepository(Ressource).delete({});
      });
    });
    ressourceMocks.forEach(ressource => {
      let created: Ressource;

      it(`Should save a new Ressource type ${ressource.type}`, async () => {
        created = await createMockRessource(ressource);
        assertRessource(created, ressource);
      });
      it(`Should update an existant Ressource type ${ressource.type}`, async () => {
        created = await createMockRessource(ressource);
        created.url = ressource.url;
        created.url = await Ressource.updateUrl(
          created.url,
          created.type,
          created.id
        );
        created.caption = ressource.caption;
        created.type = ressource.type;
        const updated = await created.save();
        assertRessource(updated, ressource);
      });
      it(`Should remove an existant Ressource type ${ressource.type}`, async () => {
        created = await createMockRessource(ressource);
        await created.remove();
        const [loaded, count] = await getRepository(Ressource).findAndCount({
          id: created.id
        });
        chai.expect(count).to.equal(0);
        chai.expect(loaded).to.be.lengthOf(0);
      });
    });
  });
});
export function assertRessource(current: any, expected: any) {
  chai.expect(current).to.be.ok;
  chai.expect(current.url).to.contain(current.id, current.type);
  chai.expect(current.caption).to.eql(expected.caption);
  chai.expect(current.type).to.eql(expected.type);
}
