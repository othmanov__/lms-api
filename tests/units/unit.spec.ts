import { Unit } from "@app/entity/Unit";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import { Lesson } from "@app/entity/Lesson";
import { Course } from "@app/entity/Course";
import { getMockCourse, getMockLesson, getMockUnit } from "tests/units.utils";
import * as Faker from "faker";
import { assertLesson } from "./lesson.spec";

const should = chai.should();
let course: Course;
let lessons: Lesson[];
let connection: Connection = null;
describe("Unit model", function() {
  let goodUnit = {
    name: "Begining algorithmics",
    course: null,
    lessons: null
  };
  before(async () => {
    connection = getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    await connection.manager.delete(Unit, {});
    if (course || lessons) {
      getRepository(Course).remove(course);
      getRepository(Lesson).remove(lessons);
    }
    course = await getMockCourse(); // prepare a course to have its units assigned to
    lessons = [await getMockLesson(), await getMockLesson()];
    goodUnit = {
      ...goodUnit,
      course,
      lessons
    };
  });
  after(async () => {
    await connection.manager.delete(Lesson, {});
    await connection.manager.delete(Course, {});
    await connection.manager.delete(Unit, {});
    await closeConnection();
  });

  it("Should  query an existing unit", async () => {
    let unit: Unit = null;
    unit = await getMockUnit(goodUnit);
    const savedUnit = await getRepository(Unit).findOne(unit.id);
    chai.expect(savedUnit, "Unit not saved").to.be.ok;
  });

  it("Should create and save a unit", async () => {
    let unit = null;
    unit = await getMockUnit(goodUnit);
    const savedUnit = await getRepository(Unit).findOneOrFail(unit.id, {
      loadEagerRelations: true
    });
    assertUnit(unit, savedUnit);
  });

  it("Should  update an existing unit", async () => {
    let unit: Unit = null;
    unit = await getMockUnit(goodUnit);
    const updatedData = {
      name: "Unit name",
      lessons: [...lessons]
    };
    unit.name = updatedData.name;
    unit.lessons = updatedData.lessons;
    await unit.save();
    let updatedUnit: Unit = await getRepository(Unit).findOneOrFail(unit.id, {
      loadEagerRelations: true
    });
    assertUnit(unit, updatedUnit);
  });

  it("Should  delete an existing unit", async () => {
    let unit: Unit = null;
    unit = await getMockUnit(goodUnit);
    const savedUnit = await getRepository(Unit).findOne(unit.id);
    await savedUnit.remove();
    const unitsCount = await getRepository(Unit).count({ id: unit.id });
    chai
      .expect(unitsCount)
      .equals(0, "Unit not deleted, count is " + unitsCount);
  });
});

export function assertUnit(expected: Unit, actual: Unit | any) {
  chai.expect(expected, "Unit not saved").to.be.ok;
  chai
    .expect(expected.name)
    .equals(actual.name, `Unit name is not the same as saved one`);
  chai
    .expect(expected.course)
    .deep.equals(actual.course, `Unit course is not the same as saved one`);
  if (expected.lessons) {
    chai.expect(actual.lessons).to.be.ok;
    chai
      .expect(expected.lessons)
      .have.lengthOf(
        actual.lessons.length,
        `Unit lessons is not the same as saved one`
      );
    expected.lessons.forEach((lesson, index) => {
      assertLesson(lesson, actual.lessons[index]);
    });
  }
}
