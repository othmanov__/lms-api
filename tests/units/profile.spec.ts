import { closeConnection, openConnection } from "@app/index";
import { ProfileDetails } from "@app/entity/ProfileDetails";
import { User } from "@app/entity/User";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { getMockUser } from "tests/units.utils";
const should = chai.should();
export const goodProfile = {
  bio: "My name is slimshady",
  phone1: "1234566789",
  facebook: "https://facebook.com/oauth3.0"
};
describe("Profile model", function() {
  let goodUserId = null;
  let connection: Connection = null;

  before(async () => {
    connection = await getConnection();
    await openConnection();
    const { id } = await getMockUser();
    goodUserId = id;
  });
  beforeEach(async () => {
    await connection.manager.delete(ProfileDetails, {});
  });
  after(async () => {
    await getRepository(User).delete({ id: goodUserId });
    await getRepository(ProfileDetails).delete({});
    await closeConnection();
  });

  it("Should create a profile and link it to a user", async () => {
    let profile = await getMockProfile(goodProfile);
    const profileCount = await getRepository(ProfileDetails).count({});
    profileCount.should.be.equal(1, "Profile not saved");
    let user = await connection.manager.findOne(User, goodUserId);
    chai.assert.ok(
      user,
      "User not found  ? what goes wrong with id " + goodUserId
    );
    user.profile = profile;
    await user.save();
    user = await connection.manager.findOne(User, goodUserId);
    chai.assert.ok(user, "User save failed");
    chai.assert.ok(user.profile, "Profile detail not saved to user");
    assertProfile(user.profile, profile);
  });
  it("Should update a profile linked to user", async () => {
    const mock = {
      bio: "Changed the bio",
      phone1: "101010101010",
      facebook: "https://facebook.com/metoo"
    };
    const profile = await getMockProfile(goodProfile);
    let user = await connection.manager.findOne(User, goodUserId);
    user.profile = profile;
    await user.save();
    chai.assert.ok(user.profile, "User save profile failed");
    user.profile.bio = mock.bio;
    user.profile.phone1 = mock.phone1;
    user.profile.facebook = mock.facebook;
    const updated = await user.save();
    user = await connection.manager.findOne(User, goodUserId);
    chai.assert.ok(updated, "User failed to update");
    chai.assert.ok(updated.profile, "Profile detail not exist");
    assertProfile(updated.profile, mock);
  });
  it("Should remove a profile linked to user", async () => {
    let user = await connection.manager.findOne(User, goodUserId);
    if (!user.profile) {
      const profile = await getMockProfile(goodProfile);
      user.profile = profile;
      await user.save();
    }
    user.profile = null;
    await user.save();
    chai.assert.isNull(user.profile);
  });
});

export async function getMockProfile(goodProfile: {
  bio: string;
  phone1: string;
  facebook: string;
}) {
  let profile = new ProfileDetails();
  profile.bio = goodProfile.bio;
  profile.phone1 = goodProfile.phone1;
  profile.facebook = goodProfile.facebook;
  return await profile.save();
}
export function assertProfile(
  actual: ProfileDetails,
  expected: ProfileDetails | any
) {
  chai.assert.equal(
    actual.bio,
    expected.bio,
    "Bios supplied not matching saved"
  );
  chai.assert.equal(
    actual.phone1,
    expected.phone1,
    "Phone 1 supplied not matching saved"
  );
  chai.assert.equal(
    actual.facebook,
    expected.facebook,
    "Facebook supplied not matching saved"
  );
}
