import { closeConnection, openConnection } from "@app/index";
import { Student } from "@app/entity/Student";
import { User } from "@app/entity/User";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection } from "typeorm";
import { getMockUser } from "tests/units.utils";
import { getMockStudent } from "tests/units.utils";
const should = chai.should();
export const goodStudent = {
  firstName: "Myaki",
  lastName: "Asumi",
  dob: new Date("1992-01-01"),
  nsc: "900317912381"
};
describe("Student model", function() {
  let user: User = null;
  let connection: Connection = null;
  before(async () => {
    connection = await getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    await connection.manager.delete(Student, {});
    await connection.manager.delete(User, {});
    user = await getMockUser();
  });

  after(async () => {
    await connection.manager.delete(Student, {});
    await connection.manager.delete(User, {});
    await closeConnection();
  });

  it("Should  query an existing student", async () => {
    let student: Student = null;
    student = await getMockStudent(goodStudent, user);
    const savedStudent = await connection
      .getRepository(Student)
      .findOne(student.id);
    chai.expect(savedStudent, "Student not saved").to.be.ok;
  });

  it("Should create and save a student", async () => {
    let student = null;
    student = await getMockStudent(goodStudent, user);
    const savedStudent = await connection
      .getRepository(Student)
      .findOne(student.id);
    assertStudent(savedStudent, goodStudent);
  });
  it("Should  update an existing student", async () => {
    let student: Student = null;
    student = await getMockStudent(goodStudent, user);
    const updatedData = {
      nsc: "1290192103",
      dob: new Date("2004-01-01"),
      firstName: "Othmane",
      lastName: "Banouzi"
    };
    student.firstName = updatedData.firstName;
    student.lastName = updatedData.lastName;
    student.dob = updatedData.dob;
    student.nsc = updatedData.nsc;
    const updatedStudent: Student = await student.save();
    assertStudent(updatedStudent, updatedData);
  });
  it("Should  delete an existing student", async () => {
    let student: Student = null;
    student = await getMockStudent(goodStudent, user);
    const savedStudent = await connection
      .getRepository(Student)
      .findOne(student.id);
    await savedStudent.remove();
    const studentCount = await connection
      .getRepository(Student)
      .count({ id: student.id });
    chai
      .expect(studentCount)
      .equals(0, "Student not deleted, count is " + studentCount);
  });
});
export function assertStudent(actual: Student, expected: Student | any) {
  chai.expect(actual, "Student not saved").to.be.ok;
  chai
    .expect(actual.firstName)
    .equals(
      expected.firstName,
      `Student firstname is not the same as saved one`
    );
  chai
    .expect(actual.lastName)
    .equals(expected.lastName, `Student lastname is not the same as saved one`);
  chai
    .expect(actual.dob.toDateString())
    .equals(
      expected.dob.toDateString(),
      `Student Date of birth is is not the same as saved one`
    );
  chai
    .expect(actual.nsc)
    .equals(expected.nsc, `Student NSC is is not the same as saved one`);
}
