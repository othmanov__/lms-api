import { Course } from "@app/entity/Course";
import { goodTeacher, assertTeacher } from "./teacher.spec";
import { getMockUser } from "tests/units/user.spec";
import {
  getMockTeacher,
  getMockEnrolment,
  getMockCourse,
  getMockStudent
} from "tests/units.utils";
import { Enrolment } from "@app/entity/Enrolment";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import { assertStudent } from "./student.spec";
import { User } from "@app/entity/User";
import { Teacher } from "@app/entity/Teacher";
import { Student } from "@app/entity/Student";
const should = chai.should();
let goodEnrolment: Partial<Enrolment> = {};
describe("Enrolment model", function() {
  let connection: Connection = null;
  before(async () => {
    connection = getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    goodEnrolment = {
      ...goodEnrolment,
      course: await getMockCourse(),
      student: await getMockStudent()
    };
  });
  after(async () => {
    await connection.manager.delete(Student, {});
    await connection.manager.delete(Course, {});
    await connection.manager.delete(Enrolment, {});
    await closeConnection();
  });

  it("Should  query an existing enrolment", async () => {
    let enrolment: Enrolment = null;
    enrolment = await getMockEnrolment(goodEnrolment);

    const savedEnrolment = await connection
      .getRepository(Enrolment)
      .findOne(enrolment.id);
    chai.expect(savedEnrolment, "Enrolment not saved").to.be.ok;
  });

  it("Should fail create  a enrolment without course", async () => {
    let enrolment: Enrolment = null;
    enrolment = await getMockEnrolment({
      ...goodEnrolment,
      course: null
    });
    try {
      await connection.getRepository(Enrolment).findOne(enrolment.id);
    } catch (error) {
      chai.expect(error).to.be.ok;
    }
  });
  it("Should fail create  a enrolment without student", async () => {
    let enrolment: Enrolment = null;
    enrolment = await getMockEnrolment({
      ...goodEnrolment,
      student: null
    });
    try {
      await connection.getRepository(Enrolment).findOne(enrolment.id);
    } catch (error) {
      chai.expect(error).to.be.ok;
    }
  });
  it("Should create and save a enrolment With a Course and a student", async () => {
    let enrolment: Enrolment = null;
    enrolment = await getMockEnrolment(goodEnrolment);
    const savedEnrolment = await connection
      .getRepository(Enrolment)
      .findOne(enrolment.id);
    assertEnrolment(savedEnrolment, enrolment);
  });
  it("Should  update an existing enrolment", async () => {
    let enrolment: Enrolment = null;
    enrolment = await getMockEnrolment(goodEnrolment);
    const updatedData: Partial<Enrolment> = {
      ...goodEnrolment,
      student: await getMockStudent(),
      course: await getMockCourse(),
      isPaid: true
    };
    enrolment.student = updatedData.student;
    enrolment.course = updatedData.course;
    enrolment.isPaid = updatedData.isPaid;
    const updatedEnrolment: Enrolment = await enrolment.save();
    assertEnrolment(enrolment, updatedEnrolment);
  });
  it("Should  delete an existing enrolment", async () => {
    let enrolment: Enrolment = null;
    enrolment = await getMockEnrolment(goodEnrolment);
    const savedEnrolment = await connection
      .getRepository(Enrolment)
      .findOne(enrolment.id);
    await savedEnrolment.remove();
    const enrolmentCount = await connection
      .getRepository(Enrolment)
      .count({ id: enrolment.id });
    chai
      .expect(enrolmentCount)
      .equals(0, "Enrolment not deleted, count is " + enrolmentCount);
  });
});
export function assertEnrolment(actual: Enrolment, expected: Enrolment | any) {
  chai.expect(actual, "Enrolment not saved").to.be.ok;
  chai.expect(actual.student, "Enrolment student not saved").to.be.ok;
  chai
    .expect(actual.student.id)
    .equals(
      expected.student.id,
      `Enrolment student is not the same as saved one`
    );
  chai.expect(actual.course, "Enrolment course not saved").to.be.ok;
  chai
    .expect(actual.course.id)
    .equals(
      expected.course.id,
      `Enrolment course is not the same as saved one`
    );
}
