import { Announcement } from "@app/entity/Announcement";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import environment from "@env";
import { expect } from "chai";
import { getMockAnnouncement } from "tests/units.utils";
const should = chai.should();
let connection: Connection = null;
describe("Announcement model", async function() {
  before(async () => {
    connection = getConnection();
  });

  beforeEach(async () => {
    await connection.manager.delete(Announcement, {});
  });

  after(async () => {
    await connection.manager.delete(Announcement, {});
    await closeConnection();
  });

  it("Should  query an existing announcement", async () => {
    let announcement: Announcement = null;
    announcement = await getMockAnnouncement();
    const savedAnnouncement = await getRepository(Announcement).findOne(
      announcement.id
    );
    chai.expect(savedAnnouncement, "Announcement not saved").to.be.ok;
  });

  it("Should create and save a announcement", async () => {
    let announcement = null;
    announcement = await getMockAnnouncement();
    const savedAnnouncement = await getRepository(Announcement).findOne(
      announcement.id
    );
    assertAnnouncement(announcement, savedAnnouncement);
  });

  it("Should  update an existing announcement", async () => {
    let announcement: Announcement = null;
    announcement = await getMockAnnouncement();
    const updatedData = {
      title: "Announcement title",
      contents: "Announcement"
    };
    announcement.title = updatedData.title;
    announcement.contents = updatedData.contents;
    await announcement.save();
    let updatedAnnouncement: Announcement = await getRepository(
      Announcement
    ).findOne(announcement.id);
    assertAnnouncement(updatedAnnouncement, updatedData);
  });

  it("Should  delete an existing announcement", async () => {
    let announcement: Announcement = null;
    announcement = await getMockAnnouncement();
    const savedAnnouncement = await getRepository(Announcement).findOne(
      announcement.id
    );
    await savedAnnouncement.remove();
    const announcementsCount = await getRepository(Announcement).count({
      id: announcement.id
    });
    chai
      .expect(announcementsCount)
      .equals(0, "Announcement not deleted, count is " + announcementsCount);
  });
});

export function assertAnnouncement(
  expected: Announcement,
  actual: Announcement | any
) {
  chai.expect(expected, "Announcement not saved").to.be.ok;
  chai
    .expect(expected.title)
    .equals(actual.title, `Announcement title is not the same as saved one`);
  chai
    .expect(expected.contents)
    .equals(
      actual.contents,
      `Announcement contents is not the same as saved one`
    );
}
