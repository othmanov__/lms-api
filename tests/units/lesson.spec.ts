import { Lesson } from "@app/entity/Lesson";
import * as chai from "chai";
import * as Faker from "faker";

import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import { getMockLesson, getMockUnit } from "tests/units.utils";
const should = chai.should();
let unit = null;
describe("Lesson model", async function() {
  unit = null;
  const goodLesson: Lesson | any = {
    title: "Lesson title",
    content: "Lesson",
    duration: 3600 * 60,
    unit
  };
  let connection: Connection = null;

  before(async () => {
    connection = getConnection();
    await openConnection();
    unit = await getMockUnit({
      name: "Computer hardware UNit",
      course: null,
      lessons: null
    });
  });

  beforeEach(async () => {
    await connection.manager.delete(Lesson, {});
  });

  after(async () => {
    await connection.manager.delete(Lesson, {});
    await closeConnection();
  });

  it("Should  query an existing lesson", async () => {
    let lesson: Lesson = null;
    lesson = await getMockLesson(goodLesson);
    const savedLesson = await getRepository(Lesson).findOne(lesson.id);
    chai.expect(savedLesson, "Lesson not saved").to.be.ok;
  });

  it("Should create and save a lesson", async () => {
    let lesson = null;
    lesson = await getMockLesson(goodLesson);
    const savedLesson = await getRepository(Lesson).findOne(lesson.id);
    assertLesson(lesson, savedLesson);
  });

  it("Should  update an existing lesson", async () => {
    let lesson: Lesson = null;
    lesson = await getMockLesson(goodLesson);
    const updatedData = {
      title: "Lesson title",
      content: "Lesson",
      duration: 3600 * 60
    };
    lesson.title = updatedData.title;
    lesson.content = updatedData.content;
    lesson.duration = updatedData.duration;
    await lesson.save();
    let updatedLesson: Lesson = await getRepository(Lesson).findOne(lesson.id);
    assertLesson(updatedLesson, updatedData);
  });

  it("Should  delete an existing lesson", async () => {
    let lesson: Lesson = null;
    lesson = await getMockLesson(goodLesson);
    const savedLesson = await getRepository(Lesson).findOne(lesson.id);
    await savedLesson.remove();
    const lessonsCount = await getRepository(Lesson).count({ id: lesson.id });
    chai
      .expect(lessonsCount)
      .equals(0, "Lesson not deleted, count is " + lessonsCount);
  });
});

export function assertLesson(expected: Lesson, actual: Lesson | any) {
  chai.expect(expected, "Lesson not saved").to.be.ok;
  chai
    .expect(expected.title)
    .equals(actual.title, `Lesson title is not the same as saved one`);
  chai
    .expect(expected.content)
    .equals(actual.content, `Lesson content is not the same as saved one`);
  chai
    .expect(expected.duration)
    .equals(actual.duration, `Lesson duration is not the same as saved one`);
}
