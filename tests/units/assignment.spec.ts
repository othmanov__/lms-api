import { AssignmentTypeEnum } from "./../../src/entity/enums";
import { Assignment } from "@app/entity/Assignment";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import { Lesson } from "@app/entity/Lesson";
import { Course } from "@app/entity/Course";
import {
  getMockCourse,
  getMockLesson,
  getMockAssignment
} from "tests/units.utils";
import * as Faker from "faker";

const should = chai.should();
let course: Course;
let connection: Connection = null;
let goodAssignment: Partial<Assignment>;
describe("Assignment model", function() {
  goodAssignment = {
    title: "Begining algorithmics",
    course: null,
    startAt: new Date(2003, 1, 1),
    endAt: new Date(2003, 1, 20),
    type: AssignmentTypeEnum.Assignment,
    instructions: "Best to have anotepad than a macbook"
  };
  before(async () => {
    connection = getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    await connection.manager.delete(Assignment, {});
    if (course) {
      getRepository(Course).remove(course);
    }
    course = await getMockCourse(); // prepare a course to have its assignments assigned to
    goodAssignment = {
      ...goodAssignment,
      course
    };
  });
  after(async () => {
    await connection.manager.delete(Course, {});
    await connection.manager.delete(Assignment, {});
    await closeConnection();
  });

  it("Should  query an existing assignment", async () => {
    let assignment: Assignment = null;
    assignment = await getMockAssignment(goodAssignment);
    const savedAssignment = await getRepository(Assignment).findOne(
      assignment.id
    );
    chai.expect(savedAssignment, "Assignment not saved").to.be.ok;
  });

  it("Should create and save a assignment", async () => {
    const assignment = await getMockAssignment(goodAssignment);
    const savedAssignment = await getRepository(Assignment).findOneOrFail(
      assignment.id
    );
    assertAssignment(assignment, savedAssignment);
  });

  it("Should  update an existing assignment", async () => {
    let assignment: Assignment = null;
    assignment = await getMockAssignment(goodAssignment);
    const updatedData: Partial<Assignment> = {
      ...goodAssignment,

      title: "Assignment title Changed",
      instructions: "Assignment instruction changed",
      startAt: new Date(2019, 2, 2),
      endAt: new Date(2019, 3, 2),
      course: await getMockCourse()
    };
    assignment.title = updatedData.title;
    assignment.instructions = updatedData.instructions;
    assignment.endAt = updatedData.endAt;
    assignment.startAt = updatedData.startAt;
    assignment.course = updatedData.course;
    await assignment.save();
    const updatedAssignment: Assignment = await getRepository(
      Assignment
    ).findOneOrFail(assignment.id);
    assertAssignment(assignment, updatedAssignment);
  });

  it("Should  delete an existing assignment", async () => {
    let assignment: Assignment = null;
    assignment = await getMockAssignment(goodAssignment);
    const savedAssignment = await getRepository(Assignment).findOne(
      assignment.id
    );
    await savedAssignment.remove();
    const assignmentsCount = await getRepository(Assignment).count({
      id: assignment.id
    });
    chai
      .expect(assignmentsCount)
      .equals(0, "Assignment not deleted, count is " + assignmentsCount);
  });
});

export function assertAssignment(
  expected: Assignment,
  actual: Assignment | any
) {
  chai.expect(expected, "Assignment not saved").to.be.ok;
  chai
    .expect(expected.title)
    .equals(actual.title, `Assignment title is not the same as saved one`);
  if (expected.course) {
    chai
      .expect(expected.course.id)
      .deep.equals(
        actual.course.id,
        `Assignment course is not the same as saved one`
      );
  }
  chai
    .expect(expected.instructions)
    .equals(
      actual.instructions,
      `Assignment instructions is not the same as saved one`
    );
  chai
    .expect(expected.startAt.toISOString())
    .equals(
      actual.startAt.toISOString(),
      `Assignment startAt is not the same as saved one`
    );
  chai
    .expect(expected.endAt.toISOString())
    .equals(
      actual.endAt.toISOString(),
      `Assignment endAt is not the same as saved one`
    );
  if (expected.acceptUntil) {
    chai
      .expect(expected.acceptUntil.toISOString())
      .equals(
        actual.acceptUntil.toISOString(),
        `Assignment acceptUntil is not the same as saved one`
      );
  }
  chai
    .expect(expected.type)
    .equals(actual.type, `Assignment type is not the same as saved one`);
}
