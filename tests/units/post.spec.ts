import { Post } from "@app/entity/Post";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import environment from "@env";
import { Ressource } from "@app/entity/Ressource";
import { expect } from "chai";
import { getMockPost, createMockRessource } from "tests/units.utils";
import { ressourceMocks } from "../units.utils";
const rimraf = require("rimraf");
const should = chai.should();

let connection: Connection = null;
let goodPost;
describe("Post model", async function() {
  goodPost = null;
  before(async () => {
    connection = getConnection();
    await openConnection();
    goodPost = {
      title: "Post name",
      content: "Post",
      medias: [
        ...(await Promise.all([
          ...ressourceMocks.map(ressource => createMockRessource(ressource))
        ]))
      ],
      categories: null
    };
  });

  beforeEach(async () => {
    await connection.manager.delete(Post, {});
  });

  after(async () => {
    await connection.manager.delete(Ressource, {});
    await connection.manager.delete(Post, {});
    await closeConnection();
    rimraf(environment.mediaBase, () => {
      console.log("Cleaning up mess");
    });
  });

  it("Should  query an existing post", async () => {
    let post: Post = null;
    post = await getMockPost(goodPost);
    const savedPost = await getRepository(Post).findOne(post.id);
    chai.expect(savedPost, "Post not saved").to.be.ok;
  });

  it("Should create and save a post", async () => {
    let post = null;
    post = await getMockPost(goodPost);
    const savedPost = await getRepository(Post).findOne(post.id);
    assertPost(post, savedPost);
  });

  it("Should  update an existing post", async () => {
    let post: Post = null;
    post = await getMockPost(goodPost);
    const updatedData = {
      title: "Post name",
      content: "Post",
      medias: [
        ...(await Promise.all([
          ...ressourceMocks.map(ressource => createMockRessource(ressource))
        ]))
      ],
      categories: []
    };
    post.title = updatedData.title;
    post.content = updatedData.content;
    post.medias = updatedData.medias;
    post.categories = updatedData.categories;
    await post.save();
    let updatedPost: Post = await getRepository(Post).findOne(post.id);
    assertPost(updatedPost, updatedData);
  });

  it("Should  delete an existing post", async () => {
    let post: Post = null;
    post = await getMockPost(goodPost);
    const savedPost = await getRepository(Post).findOne(post.id);
    await savedPost.remove();
    const postsCount = await getRepository(Post).count({ id: post.id });
    chai
      .expect(postsCount)
      .equals(0, "Post not deleted, count is " + postsCount);
  });
});

export function assertPost(expected: Post, actual: Post | any) {
  chai.expect(expected, "Post not saved").to.be.ok;
  chai
    .expect(expected.title)
    .equals(actual.title, `Post title is not the same as saved one`);
  chai
    .expect(expected.content)
    .equals(actual.content, `Post content is not the same as saved one`);
  if (expected.medias) {
    expect(expected.medias).to.have.lengthOf(actual.medias.length);
    expected.medias.forEach((media, index) => {
      expect(media.url).to.equal(
        actual.medias.find(am => am.type === media.type).url
      );
    });
  }
  if (expected.categories) {
    expect(expected.categories).to.have.lengthOf(actual.categories.length);
    expect(expected.categories).to.have.members(actual.categories);
  }
}
