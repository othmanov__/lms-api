import { Course } from "@app/entity/Course";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import { getMockCourse } from "tests/units.utils";
const should = chai.should();

describe("Course model", function() {
  let connection: Connection = null;

  before(async () => {
    connection = getConnection();
    await openConnection();
  });

  beforeEach(async () => {
    await connection.manager.delete(Course, {});
  });

  after(async () => {
    await connection.manager.delete(Course, {});
    await closeConnection();
  });

  it("Should  query an existing course", async () => {
    let course: Course = null;
    course = await getMockCourse();
    const savedCourse = await getRepository(Course).findOne(course.id);
    chai.expect(savedCourse, "Course not saved").to.be.ok;
  });

  it("Should create and save a course", async () => {
    let course = null;
    course = await getMockCourse();
    const savedCourse = await getRepository(Course).findOne(course.id);
    assertCourse(course, savedCourse);
  });

  it("Should  update an existing course", async () => {
    let course: Course = null;
    course = await getMockCourse();
    const updatedData = {
      name: "Course name",
      subject: "Course",
      description: "lorem lipsum amet course ",
      startAt: new Date("2012-01-01"),
      endAt: new Date("2012-04-01")
    };
    course.name = updatedData.name;
    course.startAt = updatedData.startAt;
    course.endAt = updatedData.endAt;
    course.subject = updatedData.subject;
    course.description = updatedData.description;
    await course.save();
    let updatedCourse: Course = await getRepository(Course).findOne(course.id);
    assertCourse(updatedCourse, updatedData);
  });

  it("Should  delete an existing course", async () => {
    let course: Course = null;
    course = await getMockCourse();
    const savedCourse = await getRepository(Course).findOne(course.id);
    await savedCourse.remove();
    const coursesCount = await getRepository(Course).count({ id: course.id });
    chai
      .expect(coursesCount)
      .equals(0, "Course not deleted, count is " + coursesCount);
  });
});

export function assertCourse(expected: Course, actual: Course | any) {
  chai.expect(expected, "Course not saved").to.be.ok;
  chai
    .expect(expected.name)
    .equals(actual.name, `Course name is not the same as saved one`);
  chai
    .expect(expected.subject)
    .equals(actual.subject, `Course subject is not the same as saved one`);
  chai
    .expect(expected.description)
    .equals(
      actual.description,
      `Course description is not the same as saved one`
    );
  chai
    .expect(expected.startAt.toISOString())
    .equals(
      actual.startAt.toISOString(),
      `Course startAt is not the same as saved one`
    );
  chai
    .expect(expected.endAt.toISOString())
    .equals(
      actual.endAt.toISOString(),
      `Course endAt is not the same as saved one}`
    );
}
