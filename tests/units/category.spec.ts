import { Category } from "@app/entity/Category";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import environment from "@env";
import { expect } from "chai";
import { getMockCategory, createMockRessource } from "tests/units.utils";
import * as path from "path";
import { Ressource } from "@app/entity/Ressource";
import { Ressource } from "@app/entity/Ressource";
const should = chai.should();
let connection: Connection = null;
let image: Ressource = null;
describe("Category model", async function() {
  before(async () => {
    connection = getConnection();
    await openConnection();
    image = await createMockRessource();
  });

  beforeEach(async () => {
    await connection.manager.delete(Category, {});
  });

  after(async () => {
    await connection.manager.delete(Ressource, {});
    await connection.manager.delete(Category, {});
    await closeConnection();
  });

  it("Should  query an existing category", async () => {
    let category: Category = null;
    category = await getMockCategory();
    const savedCategory = await getRepository(Category).findOne(category.id);
    chai.expect(savedCategory, "Category not saved").to.be.ok;
  });

  it("Should create and save a category", async () => {
    let category = null;
    category = await getMockCategory();
    const savedCategory = await getRepository(Category).findOne(category.id);
    assertCategory(category, savedCategory);
  });

  it("Should  update an existing category", async () => {
    let category: Category = null;
    category = await getMockCategory();
    const updatedData: Partial<Category> = {
      name: "Category name",
      description: "Category",
      image
    };
    category.name = updatedData.name;
    category.description = updatedData.description;
    category.image = updatedData.image;
    await category.save();
    const updatedCategory: Category = await getRepository(Category).findOne(
      category.id
    );
    assertCategory(updatedCategory, updatedData);
  });

  it("Should  delete an existing category", async () => {
    let category: Category = null;
    category = await getMockCategory();
    const savedCategory = await getRepository(Category).findOne(category.id);
    await savedCategory.remove();
    const categorysCount = await getRepository(Category).count({
      id: category.id
    });
    chai
      .expect(categorysCount)
      .equals(0, "Category not deleted, count is " + categorysCount);
  });
});

export function assertCategory(expected: Category, actual: Category | any) {
  chai.expect(expected, "Category not saved").to.be.ok;
  chai
    .expect(expected.name)
    .equals(actual.name, `Category name is not the same as saved one`);
  chai
    .expect(expected.description)
    .equals(
      actual.description,
      `Category description is not the same as saved one`
    );
  if (expected.image) {
    chai.expect(actual.image).to.be.ok;
  }
}
