import { goodTeacher, assertTeacher } from "./teacher.spec";
import { getMockUser } from "tests/units/user.spec";
import { getMockTeacher, getMockGroup } from "tests/units.utils";
import { Group } from "@app/entity/Group";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
import { assertStudent } from "./student.spec";
import { User } from "@app/entity/User";
import { Teacher } from "@app/entity/Teacher";
const should = chai.should();
export const goodGroup: Group | any = {
  name: "Computer science group",
  admin: null,
  members: []
};
describe("Group model", function() {
  let connection: Connection = null;
  before(async () => {
    connection = getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    await connection.manager.delete(Group, {});
  });
  after(async () => {
    await connection.manager.delete(Group, {});
    await connection.manager.delete(Teacher, {});
    await connection.manager.delete(User, {});
    await closeConnection();
  });

  it("Should  query an existing group", async () => {
    let group: Group = null;
    group = await getMockGroup(goodGroup);
    const savedGroup = await connection.getRepository(Group).findOne(group.id);
    chai.expect(savedGroup, "Group not saved").to.be.ok;
  });

  it("Should create and save a group without members/admin", async () => {
    let group: Group = null;
    group = await getMockGroup(goodGroup);
    const savedGroup = await connection.getRepository(Group).findOne(group.id);
    assertGroup(group, savedGroup);
  });
  it("Should create and save a group With members/admin", async () => {
    let group: Group = null;
    group = await getMockGroup({
      ...goodGroup,
      admin: await getMockTeacher(
        goodTeacher,
        await getMockUser({
          username: "sensei",
          email: "othmaneopen-15@hotmail.com",
          password: "alphatest",
          role: "moderator"
        })
      )
    });
    const savedGroup = await connection.getRepository(Group).findOne(group.id);
    assertGroup(savedGroup, group);
  });
  it("Should  update an existing group", async () => {
    let group: Group = null;
    group = await getMockGroup(goodGroup);
    const updatedData = {
      name: "Arts Group"
    };
    group.name = updatedData.name;
    const updatedGroup: Group = await group.save();
    assertGroup(group, updatedGroup);
  });
  it("Should  delete an existing group", async () => {
    let group: Group = null;
    group = await getMockGroup(goodGroup);
    const savedGroup = await connection.getRepository(Group).findOne(group.id);
    await savedGroup.remove();
    const groupCount = await connection
      .getRepository(Group)
      .count({ id: group.id });
    chai
      .expect(groupCount)
      .equals(0, "Group not deleted, count is " + groupCount);
  });
});
export function assertGroup(actual: Group, expected: Group | any) {
  chai.expect(actual, "Group not saved").to.be.ok;
  chai
    .expect(actual.name)
    .equals(
      expected.name,
      `Group name is not the same as saved one ${expected.name}`
    );
  chai.expect(actual.members).to.be.same.lengthOf(expected.members);
  for (let i = 0; i < actual.members.length; i++) {
    const actualStudent = actual.members[i];
    const expectedStudent = expected.members[i];
    assertStudent(actualStudent, expectedStudent);
  }
  if (actual.admin && expected.admin) {
    assertTeacher(actual.admin, expected.admin);
  }
}
