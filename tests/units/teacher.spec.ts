import { Teacher } from "@app/entity/Teacher";
import { User } from "@app/entity/User";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection, getRepository } from "typeorm";
import { closeConnection, openConnection } from "@app/index";
import { getMockUser, getMockTeacher } from "tests/units.utils";
const should = chai.should();
export const goodTeacher = {
  firstName: "Myaki",
  lastName: "Asumi",
  dob: new Date("1992-01-01"),
  gov_number: "900317912381"
};
describe("Teacher model", function() {
  let user: User = null;

  let connection: Connection = null;
  before(async () => {
    connection = getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    await connection.manager.delete(Teacher, {});
    await connection.manager.delete(User, {});
    user = await getMockUser();
  });
  after(async () => {
    await connection.manager.delete(Teacher, {});
    await connection.manager.delete(User, {});
    await closeConnection();
  });

  it("Should  query an existing teacher", async () => {
    let teacher: Teacher = null;
    teacher = await getMockTeacher(goodTeacher, user);
    const savedTeacher = await getRepository(Teacher).findOne(teacher.id);
    chai.expect(savedTeacher, "Teacher not saved").to.be.ok;
  });

  it("Should create and save a teacher", async () => {
    let teacher = null;
    teacher = await getMockTeacher(goodTeacher, user);
    const savedTeacher = await getRepository(Teacher).findOne(teacher.id);
    assertTeacher(savedTeacher, goodTeacher);
  });

  it("Should  update an existing teacher", async () => {
    let teacher: Teacher = null;
    teacher = await getMockTeacher(goodTeacher, user);
    const updatedData = {
      gov_number: "1290192103",
      dob: new Date("2004-01-01"),
      firstName: "Othmane",
      lastName: "Banouzi"
    };
    teacher.firstName = updatedData.firstName;
    teacher.lastName = updatedData.lastName;
    teacher.dob = updatedData.dob;
    teacher.gov_number = updatedData.gov_number;
    const updatedTeacher: Teacher = await teacher.save();
    assertTeacher(updatedTeacher, updatedData);
  });
  it("Should  delete an existing teacher", async () => {
    let teacher: Teacher = null;
    teacher = await getMockTeacher(goodTeacher, user);
    const savedTeacher = await getRepository(Teacher).findOne(teacher.id);
    await savedTeacher.remove();
    const studentCount = await getRepository(Teacher).count({ id: teacher.id });
    chai
      .expect(studentCount)
      .equals(0, "Teacher not deleted, count is " + studentCount);
  });
});
export function assertTeacher(expected: Teacher, actual: Teacher | any) {
  chai.expect(expected, "Teacher not saved").to.be.ok;
  chai
    .expect(expected.firstName)
    .equals(actual.firstName, `Teacher firstname is not the same as saved one`);
  chai
    .expect(expected.lastName)
    .equals(actual.lastName, `Teacher lastname is not the same as saved one`);
  chai
    .expect(expected.dob.toISOString())
    .equals(
      actual.dob.toISOString(),
      `Teacher Date of birth is is not the same as saved one`
    );
  chai
    .expect(expected.gov_number)
    .equals(actual.gov_number, `Teacher NSC is is not the same as saved one`);
}
