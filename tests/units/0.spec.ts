import { createConnection, Connection } from "typeorm";
import { expect } from "chai";
describe("Opening a connection", function() {
  let connection: Connection = null;
  before(async () => {
    connection = await createConnection();
  });
  it("Should create a connection", async () => {
    expect(connection, "Connection not intialized").to.be.ok;
    expect(connection.isConnected, "Connection not established").to.be.true;
  });
});
