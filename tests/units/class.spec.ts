import { Class } from "@app/entity/Class";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection } from "typeorm";
import { openConnection, closeConnection } from "@app/index";
const should = chai.should();
import { getMockClass } from "tests/units.utils";
export const goodClass: Class | any = {
  name: "Computer science 1",
  code: "CS1"
};
describe("Class model", function() {
  let connection: Connection = null;
  before(async () => {
    connection = getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    await connection.manager.delete(Class, {});
  });
  after(async () => {
    await connection.manager.delete(Class, {});
    await closeConnection();
  });

  it("Should  query an existing klass", async () => {
    let klass: Class = null;
    klass = await getMockClass(goodClass);
    const savedClass = await connection.getRepository(Class).findOne(klass.id);
    chai.expect(savedClass, "Class not saved").to.be.ok;
  });

  it("Should create and save a klass", async () => {
    let klass = null;
    klass = await getMockClass(goodClass);
    const savedClass = await connection.getRepository(Class).findOne(klass.id);
    assertKlass(savedClass, goodClass);
  });
  it("Should  update an existing klass", async () => {
    let klass: Class = null;
    klass = await getMockClass(goodClass);
    const updatedData = {
      name: "Arts 1",
      code: "ar1"
    };
    klass.name = updatedData.name;
    klass.code = updatedData.code;
    const updatedClass: Class = await klass.save();
    assertKlass(updatedClass, updatedData);
  });
  it("Should  delete an existing klass", async () => {
    let klass: Class = null;
    klass = await getMockClass(goodClass);
    const savedClass = await connection.getRepository(Class).findOne(klass.id);
    await savedClass.remove();
    const studentCount = await connection
      .getRepository(Class)
      .count({ id: klass.id });
    chai
      .expect(studentCount)
      .equals(0, "Class not deleted, count is " + studentCount);
  });
});
function assertKlass(actual: Class, expected: Class | any) {
  chai.expect(actual, "Class not saved").to.be.ok;
  chai
    .expect(actual.name)
    .equals(
      expected.name,
      `Class name is not the same as saved one ${expected.name}`
    );
  chai
    .expect(actual.code)
    .equals(
      expected.code,
      `Class code is not the same as saved one ${expected.code}`
    );
}
