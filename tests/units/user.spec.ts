import { ProfileDetails } from "@app/entity/ProfileDetails";
import { User, UserRoles } from "@app/entity/User";
import * as chai from "chai";
import "mocha";
import { Connection, getConnection } from "typeorm";
import { validate } from "class-validator";
import * as bcrypt from "bcryptjs";
import { closeConnection, openConnection } from "@app/index";
import { expect } from "chai";
const should = chai.should();
let connection: Connection = null;
export const goodUser = {
  username: "othmane",
  email: "lateg15@gmail.com",
  password: "test",
  role: "admin"
};
export async function getMockUser(userData = goodUser) {
  let user = new User();
  user.username = userData.username;
  user.password = userData.password;
  user.role = userData.role;
  user.email = userData.email;
  await user.hashPassword();
  await user.save();
  return user;
}
describe("User model", function() {
  before(async () => {
    connection = await getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    connection.manager.delete(ProfileDetails, {});
    connection.manager.delete(User, {});
  });
  afterEach(async () => {
    connection.manager.delete(ProfileDetails, {});
    connection.manager.delete(User, {});
  });
  after(async () => {
    await connection.getRepository(ProfileDetails).delete({});
    await connection.getRepository(User).delete({});
    await closeConnection();
  });
  it("Should create a user with required in the database", async () => {
    const user$ = await connection.getRepository(User).create(goodUser);
    expect(user$).to.be.ok;
    await user$.hashPassword();
    let user = await user$.save();
    user.should.be.ok;
    const userCount = await connection.getRepository(User).count({});
    userCount.should.be.equal(1, "User not saved");
  });
  it("Should fail saving a user with not all required fields", async () => {
    let user = new User();
    user.username = "test me";
    const errors = await validate(user);
    errors.length.should.be.least(
      1,
      "User saved without required validation errors"
    );
    try {
      user = await user.save();
      chai.assert.notOk(
        user,
        "User must not be saved after not being validated"
      );
    } catch (error) {
      chai.assert.ok(error);
    }
  });
  it("Should fail saving a user with empty fields", async () => {
    let user = new User();
    user.username = "";
    user.email = "";
    user.password = "";
    user.role = "";
    const errors = await validate(user);
    errors.length.should.be.least(
      1,
      "User saved without empty validation errors"
    );
    try {
      user = await user.save();
      chai.assert.notOk(
        user,
        "User must not be saved after not being validated"
      );
    } catch (error) {
      chai.assert.ok(error);
    }
  });
  it("Should succed saving a user with profile details", async () => {
    let user = new User();
    user.username = "test";
    user.email = "test@example.com";
    user.password = "test";
    user.role = UserRoles.Moderator;
    const details = new ProfileDetails();
    user.profile = details;
    user = await user.save();
    chai.expect(user.profile, "Profile not saved for user" + user.username).to
      .be.ok;
  });
  it("Should update a user", async () => {
    try {
      let user = await connection.getRepository(User).save(goodUser);
      user.should.be.ok;
      user.username = "updated";
      user.email = "othmaneabanouzi@gmail.com";
      user.role = "guest";
      user.password = "newpassword";
      await user.hashPassword();
      await connection.getRepository(User).save(user); // TODO: this is not triggering the User.AfterUpdate hook
      const updated = await connection.getRepository(User).findOne({
        username: "updated",
        role: "guest"
      });
      updated.username.should.eq("updated");
      chai.assert.equal(
        await bcrypt.compare("newpassword", updated.password),
        true,
        "Password not match new one set"
      );
      user.role.should.be.equal("guest");
    } catch (error) {
      chai.assert.ok(error);
    }
  });
  it("Should delete a user", async () => {
    try {
      const user = await connection.getRepository(User).save(goodUser);
      await user.remove();
      const userCount = await connection.getRepository(User).count();
      chai.assert.equal(userCount, 1);
    } catch (error) {
      chai.assert.ok(error);
    }
  });
});
export function assertUser(expected: User, actual: User) {
  chai.expect(expected).to.be.ok;
  chai.expect(expected.username).equals(actual.username);
  if (expected.profile) {
    chai.expect(expected.profile).equals(actual.profile);
  }
  chai.expect(expected.password).equals(actual.password);
}
