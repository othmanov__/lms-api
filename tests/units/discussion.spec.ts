import { Category } from "@app/entity/Category";
import { Discussion } from "@app/entity/Discussion";
import { User, UserRoles } from "@app/entity/User";
import { closeConnection, openConnection } from "@app/index";
import * as chai from "chai";
import "mocha";
import {
  getMockCourse,
  getMockDiscussion,
  getMockCategory
} from "tests/units.utils";
import { getMockUser } from "tests/units/user.spec";
import { Connection, getConnection, getRepository } from "typeorm";
import { assertUser } from "./user.spec";
const should = chai.should();
let userMember: User = null;
let category: Category = null;
export const goodDiscussion: Partial<Discussion> = {
  topic: "Computer science discussion",
  members: null,
  course: null
};
describe("Discussion model", function() {
  let connection: Connection = null;
  before(async () => {
    connection = getConnection();
    await openConnection();
  });
  beforeEach(async () => {
    category = await getMockCategory();
    goodDiscussion.category = category;
    userMember = await getMockUser({
      username: "sensei",
      email: "othmaneopen-15@hotmail.com",
      password: "alphatest",
      role: UserRoles.Moderator
    });
    goodDiscussion.members = [userMember];
  });
  afterEach(async () => {
    if (userMember) {
      await userMember.remove();
    }
    await connection.manager.delete(Discussion, {});
  });
  after(async () => {
    await connection.manager.delete(Discussion, {});
    await closeConnection();
  });

  it("Should  query an existing discussion", async () => {
    let discussion: Discussion = null;
    discussion = await getMockDiscussion(goodDiscussion);
    const savedDiscussion = await connection
      .getRepository(Discussion)
      .findOne(discussion.id);
    chai.expect(savedDiscussion, "Discussion not saved").to.be.ok;
  });

  it("Should fail saving a discussion without members", async () => {
    let discussion: Discussion = null;
    discussion = await getMockDiscussion({
      ...goodDiscussion,
      members: null
    });
    try {
      await connection.getRepository(Discussion).findOne(discussion.id);
    } catch (error) {
      chai.expect(error).to.be.ok;
    }
  });
  it("Should create and save a discussion With at least a member", async () => {
    let discussion: Discussion = null;
    discussion = await getMockDiscussion(goodDiscussion);
    const savedDiscussion = await connection
      .getRepository(Discussion)
      .findOne(discussion.id);
    assertDiscussion(savedDiscussion, discussion);
  });
  it("Should  update an existing discussion", async () => {
    let discussion: Discussion = null;
    discussion = await getMockDiscussion(goodDiscussion);
    const updatedData = {
      topic: "ArtsDiscussion",
      course: await getMockCourse()
    };
    discussion.topic = updatedData.topic;
    discussion.course = updatedData.course;
    const updatedDiscussion: Discussion = await discussion.save();
    assertDiscussion(discussion, updatedDiscussion);
  });
  it("Should  delete an existing discussion", async () => {
    let discussion: Discussion = null;
    discussion = await getMockDiscussion(goodDiscussion);
    const savedDiscussion = await connection
      .getRepository(Discussion)
      .findOne(discussion.id);
    await savedDiscussion.remove();
    const discussionCount = await connection
      .getRepository(Discussion)
      .count({ id: discussion.id });
    chai
      .expect(discussionCount)
      .equals(0, "Discussion not deleted, count is " + discussionCount);
  });
});
export function assertDiscussion(actual: Discussion, expected: Discussion) {
  chai.expect(actual, "Discussion not saved").to.be.ok;
  chai
    .expect(actual.topic)
    .equals(expected.topic, `Discussion topic is not the same as saved one`);
  chai.expect(actual.members).to.have.lengthOf(expected.members.length);
  for (let i = 0; i < actual.members.length; i++) {
    const actualMember = actual.members[i];
    const expectedMember = expected.members[i];
    assertUser(actualMember, expectedMember);
  }
}
