import { getMockTeacher } from "tests/units.utils";
import { Group } from "./../src/entity/Group";
import { Enrolment } from "@app/entity/Enrolment";
import { AssignmentTypes } from "./../src/entity/Assignment";
import * as Faker from "faker";
import { User, UserRoles } from "@app/entity/User";
import { Class } from "@app/entity/Class";
import { Student } from "@app/entity/Student";
import { Teacher } from "@app/entity/Teacher";
import { Group } from "@app/entity/Group";
import { Course } from "@app/entity/Course";
import { Lesson } from "@app/entity/Lesson";
import { Post } from "@app/entity/Post";
import { Unit } from "@app/entity/Unit";
import { Category } from "@app/entity/Category";
import { Ressource } from "@app/entity/Ressource";
import { Announcement } from "@app/entity/Announcement";
import environment from "@env";
import * as path from "path";
import { RessourceTypeEnum } from "@app/entity/enums";
import { MongoClient } from "typeorm";
import { Assignment } from "@app/entity/Assignment";
import { Discussion } from "@app/entity/Discussion";

export const goodUser = require("mocks/auth");
export function getFakeUser(userRole = UserRoles.Admin) {
  return {
    username: Faker.internet.userName(),
    password: Faker.internet.password(),
    role: userRole,
    email: `${Faker.name.firstName()}@examples.com`.toLowerCase()
  } as Partial<User>;
}
export async function getMockUser(userData = goodUser) {
  let user = new User();
  user.username = userData.username;
  user.password = userData.username;
  user.role = userData.role;
  user.email = userData.email;
  await user.hashPassword();
  await user.save();
  return user;
}
export function codeGen(length) {
  let min;
  let max = Math.pow(10, length);
  min = max - Math.pow(10, length - 1);
  return Faker.random.number({ min, max }).toString();
}

export async function getMockTeacher(
  goodTeacher: Partial<Teacher> = {
    firstName: Faker.name.firstName(),
    lastName: Faker.name.lastName(),
    dob: Faker.date.past(19),
    nsc: codeGen(10),
    profile: null
  },
  user?: User
) {
  let teacher = new Teacher();
  teacher.firstName = goodTeacher.firstName;
  teacher.lastName = goodTeacher.lastName;
  teacher.dob = goodTeacher.dob;
  teacher.gov_number = goodTeacher.gov_number;
  teacher.profile =
    goodTeacher.user ||
    user ||
    (await getMockUser(getFakeUser(UserRoles.Teacher)));
  await teacher.save();
  return teacher;
}
export async function getMockStudent(
  goodStudent: Partial<Student> = getFakeStudent(),
  user: User = null
) {
  let student = new Student();
  student.firstName = goodStudent.firstName;
  student.lastName = goodStudent.lastName;
  student.dob = goodStudent.dob;
  student.nsc = goodStudent.nsc;
  student.profile =
    goodStudent.profile ||
    user ||
    (await getMockUser(getFakeUser(UserRoles.Student)));
  return await student.save();
}
export function getFakeStudent(): Partial<Student> {
  return {
    firstName: Faker.name.firstName(),
    lastName: Faker.name.lastName(),
    dob: Faker.date.past(19),
    nsc: codeGen(10),
    profile: null
  };
}

export async function getMockGroup(
  obj: Partial<Group> = {
    name: Faker.random.words(2),
    description: Faker.random.words(10),
    members: [],
    admin: null
  }
) {
  let group = new Group();
  group.name = obj.name;
  group.description = obj.description;
  group.closed = obj.closed;
  group.members = obj.members || [await getMockStudent()];
  group.admin = obj.admin || (await getMockTeacher());
  await group.save();
  return group;
}
export async function getMockClass(goodClass: Class | any) {
  let klass = new Class();
  klass.name = goodClass.name;
  klass.code = goodClass.code;
  klass.students = goodClass.students;
  klass.teachers = goodClass.teachers;
  await klass.save();
  return klass;
}
export async function getMockGroup(goodGroup: Group | any) {
  let group = new Group();
  group.name = goodGroup.name;
  group.members = goodGroup.members;
  group.admin = goodGroup.admin;
  await group.save();
  return group;
}
export async function getMockCourse(obj: Course = getFakeCourse()) {
  let course = new Course();
  course.name = obj.name;
  course.subject = obj.subject;
  course.description = obj.description;
  course.startAt = obj.startAt;
  course.endAt = obj.endAt;
  return await course.save();
}
export function getFakeCourse(): Course {
  return {
    name: Faker.company.catchPhrase(),
    subject: Faker.commerce.department(),
    description: Faker.random.words(10),
    startAt: Faker.date.recent(1),
    endAt: Faker.date.future(1)
  };
}

export async function getMockLesson(obj = getFakeLesson()) {
  let lesson = new Lesson();
  lesson.title = obj.title;
  lesson.content = obj.content;
  lesson.duration = obj.duration;
  lesson.unit = obj.unit;
  lesson.ressources = obj.ressources || [await getMockRessource()];
  return await lesson.save();
}

export function getFakeLesson() {
  return {
    title: Faker.lorem.text(),
    content: Faker.lorem.paragraph(),
    duration: Faker.random.number({ max: 36 * 12000 }),
    unit: null,
    ressources: null
  };
}

export async function getMockPost(obj: Post = getFakePost()) {
  let post = new Post();
  post.title = obj.title;
  post.content = obj.content;
  post.medias = obj.medias || [await getMockRessource()];
  post.categories = obj.categories || [await getMockCategory()];
  return await post.save();
}

export function getFakePost(): Post {
  return {
    title: Faker.lorem.words(5),
    content: Faker.lorem.words(10),
    medias: null,
    categories: null
  };
}

export async function getMockUnit(obj = getFakeUnit()) {
  let unit = new Unit();
  unit.name = obj.name;
  unit.course = obj.course;
  unit.lessons = obj.lessons;
  return await unit.save();
}
export function getFakeUnit() {
  return {
    name: Faker.name.jobTitle(),
    course: null,
    lessons: null
  };
}

export async function getMockCategory(obj: Category = getFakeCategory()) {
  let category = new Category();
  category.name = obj.name;
  category.description = obj.description;
  category.image = obj.image || (await getMockRessource());
  return await category.save();
}
export function getFakeCategory(): Category {
  return {
    name: Faker.lorem.word(),
    description: Faker.random.words(10),
    image: null
  };
}

export async function getMockAnnouncement(
  obj: Announcement = {
    title: Faker.lorem.words(5),
    contents: Faker.lorem.words(10),
    tags: [].fill(Faker.random.word(), 0, 3)
  }
) {
  let announcement = new Announcement();
  announcement.title = obj.title;
  announcement.contents = obj.contents;
  announcement.importance = obj.importance;
  announcement.notes = obj.notes;
  announcement.tags = obj.tags;
  announcement.audience = obj.audience;
  return await announcement.save();
}
export async function getMockAssignment(obj: Assignment = getFakeAssignment()) {
  let assignment = new Assignment();
  assignment.title = obj.title;
  assignment.course = obj.course;
  assignment.instructions = obj.instructions;
  assignment.type = obj.type;
  assignment.acceptUntil = obj.acceptUntil;
  assignment.endAt = obj.endAt;
  assignment.startAt = obj.startAt;
  return await assignment.save();
}

export function getFakeAssignment(): Assignment {
  return {
    title: Faker.lorem.words(5),
    instructions: Faker.lorem.words(10),
    type: Faker.random.arrayElement(AssignmentTypes),
    acceptUntil: Faker.date.future(0.2),
    startAt: Faker.date.recent(10),
    endAt: Faker.date.future(0.1),
    course: null
  };
}
export function getFakeEnrolment() {
  return {
    student: null,
    course: null,
    isPaid: Faker.random.boolean(),
    isExpired: Faker.random.boolean(),
    expirationDate: Faker.date.future(1.3)
  } as Enrolment;
}
export async function getMockEnrolment(obj: Enrolment = {}) {
  let enrolment = new Enrolment();
  enrolment.student = obj.student;
  enrolment.course = obj.course;
  enrolment.isPaid = obj.isPaid;
  enrolment.isExpired = obj.isExpired;
  enrolment.expirationDate = obj.expirationDate;
  return await enrolment.save();
}

export async function getMockDiscussion(
  obj: Discussion = {
    topic: Faker.random.words(10),
    course: null,
    members: null
  }
) {
  let discussion = new Discussion();
  discussion.topic = obj.topic;
  discussion.course = obj.course;
  discussion.members = obj.members;
  return await discussion.save();
}
export const mocksBaseDir = path.join(
  environment.baseDir,
  process.env.MOCKS_DIR
);

export const ressourceMocks = require("mocks/ressource/ressource.json").map(
  mock => {
    return {
      url: path.join(mocksBaseDir, "ressource", mock.url),
      caption: mock.caption,
      type: mock.type
    };
  }
);
export const badRessourceMocks = require("mocks/ressource/badRessource.json").map(
  mock => {
    return {
      url: path.join(mocksBaseDir, "ressource", mock.url),
      caption: mock.caption,
      type: mock.type
    };
  }
);
export const ressourceMocksTypes = ressourceMocks.map(m => m.type);
export function getMockRessource(type: RessourceTypeEnum) {
  return (
    ressourceMocks.find(mock => mock.type == type) || {
      url: path.join(mocksBaseDir, "ressource", "mock.jpg"),
      type: RessourceTypeEnum.JPG,
      caption: Faker.random.words(10)
    }
  );
}

export async function createMockRessource(obj: Ressource = getMockRessource()) {
  let ressource = new Ressource();
  ressource.url = obj.url;
  ressource.caption = obj.caption;
  ressource.type = obj.type;
  const { id } = await ressource.save();
  ressource.url = await Ressource.updateUrl(obj.url, obj.type, id, false);
  return await ressource.save();
}
