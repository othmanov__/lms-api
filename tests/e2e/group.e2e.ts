import { Group } from "@app/entity/Group";
import { getRepository, Not } from "typeorm";
import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { User, UserRoles } from "@app/entity/User";
import * as faker from "faker";
import "mocha";
import * as chai from "chai";
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import {
  getMockUser,
  getMockTeacher,
  getMockStudent,
  getMockGroup
} from "tests/units.utils";
import { authenticate } from "tests/e2e.utils";
import { Teacher } from "@app/entity/Teacher";
import { Student } from "@app/entity/Student";
const host = environment.api;
let students = [];
let admin = null;
describe("routes#Groups", function() {
  let authorization_token = null;
  let theServer: Server;
  const getMock = () => {
    return {
      name: faker.internet.userName(),
      members: [],
      admin: null
    } as Group;
  };
  before(async () => {
    theServer = await createExpressApp();
    await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
    try {
      admin = await getMockTeacher();
      students = [await getMockStudent()];
    } catch (error) {
      throw error;
    }
  });
  beforeEach(async () => {});
  after(async function() {
    await getRepository(Group).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  it(`should list groups`, async function() {
    const response = await send("get", routes.Groups, authorization_token);
    chai.expect(response).to.have.status(200);
  });
  it(`should save group`, async function() {
    const mock = getMock();
    const response = await send(
      "post",
      routes.Groups,
      authorization_token,
      mock
    );
    chai.expect(response).to.have.status(201);
  });
  it(`should save group with admin and members`, async function() {
    const mock = {
      ...getMock(),
      members: students,
      admin
    };
    const response = await send(
      "post",
      routes.Groups,
      authorization_token,
      mock
    );
    chai.expect(response).to.have.status(201);
  });
  it(`should update an existing group`, async function() {
    const mock = getMock();
    const mockReplacement = getMock();
    const saved = await getMockGroup(mock);
    const response = await send(
      "patch",
      routes.Groups,
      authorization_token,
      mockReplacement,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
  it(`should delete an existing group`, async function() {
    const mock = getMock();
    const saved = await getMockGroup(mock);
    const response = await send(
      "delete",
      routes.Groups,
      authorization_token,
      null,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
});
