import { Category } from "@app/entity/Category";
import { Post } from "@app/entity/Post";
import { getRepository, Not } from "typeorm";
import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { User, UserRoles } from "@app/entity/User";
import "mocha";
import * as chai from "chai";
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import {
  getMockPost,
  getMockPost,
  getFakePost,
  getMockCategory
} from "tests/units.utils";
import { authenticate } from "tests/e2e.utils";
import { Teacher } from "@app/entity/Teacher";
import { Student } from "@app/entity/Student";
const host = environment.api;
describe("routes#Posts", function() {
  let authorization_token = null;
  let theServer: Server;
  let category: Category;
  const getMock = () => {
    return getFakePost();
  };
  before(async () => {
    theServer = await createExpressApp();
    await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
    category = await getMockCategory();
  });
  after(async function() {
    await category.remove();
    await getRepository(Post).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  it(`should list posts`, async function() {
    const response = await send("get", routes.Posts, authorization_token);
    chai.expect(response).to.have.status(200);
  });
  it(`should save post`, async function() {
    const mock: Post = {
      ...getMock(),
      categories: [category]
    };
    const response = await send(
      "post",
      routes.Posts,
      authorization_token,
      mock
    );
    chai.expect(response).to.have.status(201);
  });
  it(`should update an existing post`, async function() {
    const mock = getMock();
    const mockReplacement = getMock();
    const saved = await getMockPost(mock);
    const response = await send(
      "patch",
      routes.Posts,
      authorization_token,
      mockReplacement,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
  it(`should delete an existing post`, async function() {
    const mock = getMock();
    const saved = await getMockPost(mock);
    const response = await send(
      "delete",
      routes.Posts,
      authorization_token,
      null,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
});
