import { Unit } from "@app/entity/Unit";
import { Course } from "@app/entity/Course";
import { getRepository, Not } from "typeorm";
import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { User, UserRoles } from "@app/entity/User";
import "mocha";
import * as chai from "chai";
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import {
  getMockCourse,
  getFakeCourse,
  getMockUnit,
  getMockLesson
} from "tests/units.utils";
import { authenticate } from "tests/e2e.utils";
import { Teacher } from "@app/entity/Teacher";
import { Student } from "@app/entity/Student";
const host = environment.api;
describe("routes#Courses", function() {
  let authorization_token = null;
  let theServer: Server;
  let unit: Unit = null;
  const getMock = () => {
    return getFakeCourse();
  };
  before(async () => {
    theServer = await createExpressApp();
    await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
    unit = await getMockUnit();
    unit.lessons = [await getMockLesson()];
    await unit.save();
  });
  after(async function() {
    await unit.remove();
    await getRepository(Course).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  it(`should list coursess`, async function() {
    const response = await send("get", routes.Courses, authorization_token);
    chai.expect(response).to.have.status(200);
  });
  it(`should save courses`, async function() {
    const mock: Course = {
      ...getMock(),
      units: [unit]
    };
    const response = await send(
      "post",
      routes.Courses,
      authorization_token,
      mock
    );
    chai.expect(response).to.have.status(201);
  });
  it(`should update an existing courses`, async function() {
    const mock: Course = getMock();
    const mockReplacement: Course = {
      ...getMock(),
      units: [unit]
    };
    const saved = await getMockCourse(mock);
    const response = await send(
      "patch",
      routes.Courses,
      authorization_token,
      mockReplacement,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
  it(`should delete an existing courses`, async function() {
    const mock = getMock();
    const saved = await getMockCourse(mock);
    const response = await send(
      "delete",
      routes.Courses,
      authorization_token,
      null,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
});
