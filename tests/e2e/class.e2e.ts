import { Class } from "@app/entity/Class";
import { User } from "@app/entity/User";
import { Logger } from "@app/helpers/logger";
import { closeConnection, createExpressApp } from "@app/index";
import * as chai from "chai";
import * as faker from "faker";
import { Server } from "http";
import "mocha";
import { send } from "tests/e2e.test";
import { authenticate } from "tests/e2e.utils";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import { getMockUser } from "tests/units.utils";
import { getMockClass } from "tests/units/class.spec";
import { getRepository, Not } from "typeorm";
import { routes } from "./../e2e.test";
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
describe("routes#Classes", function() {
  let authorization_token = null;
  let theServer: Server;
  const getMock = () => {
    return {
      name: faker.random.alphaNumeric(100),
      code: faker.random.alphaNumeric(5),
      students: null,
      teachers: null
    } as Class;
  };
  before(async () => {
    console.log("entred before class hook");

    theServer = await createExpressApp();
    await createUserOrDefault();
    console.log("USer createed");
    authorization_token = await authenticate(getDefaultUserAuths());
    console.log(authorization_token);
  });
  beforeEach(async () => {
    await getRepository(User).delete({
      username: Not("admin") /// all Classes except the logged in one for api
    });
  });
  after(async function() {
    await getRepository(Class).delete({});
    await getRepository(User).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  /*  describe("Best scenario", function() {
    it(`should list students`, async function() {
      const response = await send("get", routes.Classes, authorization_token);
      await getMockClass(getMock());
      chai.expect(response.body).to.be.lengthOf(1);
    });
    it(`should save a class`, async function() {
      const mock = getMock();
      const response = await send(
        "post",
        routes.Classes,
        authorization_token,
        mock
      );
      chai.expect(response).to.have.status(201);
    });
    it(`should update an existing class`, async function() {
      const mock = getMock();
      const mockReplacement = getMock();
      const saved = await getMockUser(mock);
      const response = await send(
        "patch",
        routes.Classes,
        authorization_token,
        mockReplacement,
        saved.id
      );
      chai.expect(response).to.have.status(204);
    });
    it(`should delete an existing class`, async function() {
      const mock = getMock();
      const saved = await getMockUser(mock);
      const response = await send(
        "delete",
        routes.Classes,
        authorization_token,
        null,
        saved.id
      );
      chai.expect(response).to.have.status(204);
    });
  });*/
});
