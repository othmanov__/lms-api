import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { Teacher } from "@app/entity/Teacher";
import * as faker from "faker";
import "mocha";
import * as chai from "chai";
const expect = chai.expect;
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { getMockTeacher } from "tests/units.utils";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { getRepository } from "typeorm";
import { User } from "@app/entity/User";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import { authenticate } from "tests/e2e.utils";
const host = environment.api;
describe("routes#Teachers", function() {
  let authorization_token = null;
  let theServer: Server;
  const getMock = () => {
    return {
      firstName: faker.name.firstName(),
      lastName: faker.name.firstName(),
      dob: faker.date.past(20),
      gov_number: faker.random
        .number({ min: Math.pow(10, 9), max: Math.pow(10, 10) })
        .toString()
    } as Teacher;
  };
  before(async () => {
    theServer = await createExpressApp();
    await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
  });
  after(async function() {
    await getRepository(Teacher).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  describe("Best scenario", function() {
    it(`should list teachers`, async function() {
      const response = await send("get", routes.Teachers, authorization_token);
      expect(response).to.have.status(200);
    });
    it(`should save teacher`, async function() {
      const mock = getMock();
      const response = await send(
        "post",
        routes.Teachers,
        authorization_token,
        mock
      );
      expect(response).to.have.status(201);
    });
    it(`should update an existing teacher`, async function() {
      const mock = getMock();
      const mockReplacement = getMock();
      const saved = await getMockTeacher(mock);
      const response = await send(
        "patch",
        routes.Teachers,
        authorization_token,
        mockReplacement,
        saved.id
      );
      expect(response).to.have.status(204);
    });
    it(`should delete an existing teacher`, async function() {
      const mock = getMock();
      const saved = await getMockTeacher(mock);
      const response = await send(
        "delete",
        routes.Teachers,
        authorization_token,
        null,
        saved.id
      );
      expect(response).to.have.status(204);
    });
  });
});
