import { Assignment } from "@app/entity/Assignment";
import { getRepository, Not } from "typeorm";
import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { User, UserRoles } from "@app/entity/User";
import "mocha";
import * as chai from "chai";
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import {
  getFakeAssignment,
  getMockCourse,
  getMockAssignment
} from "tests/units.utils";
import { authenticate } from "tests/e2e.utils";
import { Teacher } from "@app/entity/Teacher";
import { Student } from "@app/entity/Student";
const host = environment.api;
let course = null;
describe("routes#Assignments", function() {
  let authorization_token = null;
  let theServer: Server;
  const getMock = () => {
    return getFakeAssignment();
  };
  before(async () => {
    theServer = await createExpressApp();
    await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
    course = await getMockCourse();
  });
  after(async function() {
    await getRepository(Assignment).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  it(`should list assignments`, async function() {
    const response = await send("get", routes.Assignments, authorization_token);
    chai.expect(response).to.have.status(200);
  });
  it(`should save assignment`, async function() {
    const mock = {
      ...getMock(),
      course
    };
    const response = await send(
      "post",
      routes.Assignments,
      authorization_token,
      mock
    );
    chai.expect(response).to.have.status(201);
  });
  it(`should update an existing assignment`, async function() {
    const mock = {
      ...getMock(),
      course
    };
    const mockReplacement = {
      ...getMock(),
      course: await getMockCourse() // another course
    };
    const saved = await getMockAssignment(mock);
    const response = await send(
      "patch",
      routes.Assignments,
      authorization_token,
      mockReplacement,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
  it(`should delete an existing assignment`, async function() {
    const mock = getMock();
    const saved = await getMockAssignment(mock);
    const response = await send(
      "delete",
      routes.Assignments,
      authorization_token,
      null,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
});
