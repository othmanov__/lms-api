import { Unit } from "@app/entity/Unit";
import { Enrolment } from "@app/entity/Enrolment";
import { getRepository, Not } from "typeorm";
import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { User, UserRoles } from "@app/entity/User";
import "mocha";
import * as chai from "chai";
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import {
  getMockEnrolment,
  getFakeEnrolment,
  getMockUnit,
  getMockLesson,
  getMockCourse,
  getMockStudent,
  getFakeStudent
} from "tests/units.utils";
import { authenticate } from "tests/e2e.utils";
import { Teacher } from "@app/entity/Teacher";
import { Student } from "@app/entity/Student";
const host = environment.api;
describe("routes#Enrolments", function() {
  let authorization_token = null;
  let theServer: Server;
  let course: Course = null;
  let student: Student = null;
  let currentLoggedinUser: User = null;
  const getMock = () => {
    return getFakeEnrolment();
  };
  before(async () => {
    theServer = await createExpressApp();
    currentLoggedinUser = await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
  });
  beforeEach(async () => {
    course = await getMockCourse();
    student = await getMockStudent(getFakeStudent(), currentLoggedinUser);
  });
  afterEach(async () => {
    await getRepository(Enrolment).delete({});
    await course.remove();
    await student.remove();
  });
  after(async function() {
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  it(`should list enrolments`, async function() {
    const response = await send("get", routes.Enrolments, authorization_token);
    chai.expect(response).to.have.status(200);
  });
  it(`should save enrolment using current logged in student `, async function() {
    const mock: Enrolment = {
      ...getMock(),
      course
    };
    const response = await send(
      "post",
      routes.Enrolments,
      authorization_token,
      mock
    );
    chai.expect(response).to.have.status(201);
  });
  it(`should save enrolment using current passed in student `, async function() {
    const mock: Enrolment = {
      ...getMock(),
      course,
      student
    };
    const response = await send(
      "post",
      routes.Enrolments,
      authorization_token,
      mock
    );
    chai.expect(response).to.have.status(201);
  });

  it(`should update an existing enrolment`, async function() {
    const mock: Enrolment = {
      ...getMock(),
      course,
      student
    };
    const mockReplacement: Enrolment = {
      ...getMock(),
      course,
      student
    };
    const saved = await getMockEnrolment(mock);
    const response = await send(
      "patch",
      routes.Enrolments,
      authorization_token,
      mockReplacement,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
  it(`should delete an existing enrolment`, async function() {
    const mock: Enrolment = {
      ...getMock(),
      course,
      student
    };
    const saved = await getMockEnrolment(mock);
    const response = await send(
      "delete",
      routes.Enrolments,
      authorization_token,
      null,
      saved.id
    );
    chai.expect(response).to.have.status(204);
  });
});
