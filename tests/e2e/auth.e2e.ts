import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { ProfileDetails } from "@app/entity/ProfileDetails";
import { User } from "@app/entity/User";
import * as chai from "chai";
const expect = chai.expect;
import chaiHttp = require("chai-http");
import * as request from "superagent";
import "mocha";
import { getRepository } from "typeorm";
import environment from "@env";
const host = environment.api;
const should = chai.should();
import { goodUser } from "tests/units.utils";
import { routes, send } from "tests/e2e.test";
import { before } from "mocha";
import { authenticate } from "tests/e2e.utils";
import { createUserOrDefault } from "tests/units.test";
chai.use(chaiHttp);
describe("routes#Auth", function() {
  let authorization_token = null;
  let server: Server;
  before(async () => {
    server = await createExpressApp();
    await createUserOrDefault(goodUser);
  });
  after(async function() {
    await getRepository(ProfileDetails).delete({});
    await getRepository(User).delete({});
    await closeConnection();
    server.close(() => {
      console.log("Server closed");
    });
  });
  context("login", function() {
    it(`should authenticate with a good username and password`, async function() {
      authorization_token = await authenticate(goodUser);
    });

    it(`should fail to authenticate with a bad username`, async function() {
      try {
        const response = await send("post", routes.Auth.Login, null, {
          username: "bad user",
          password: goodUser.password
        });
        expect(response).to.have.status(401);
      } catch (error) {
        expect(error).to.be.ok;
      }
    });

    it(`should fail to authenticate with a bad password`, async function() {
      try {
        const response = await send("post", routes.Auth.Login, null, {
          username: goodUser.username,
          password: "test [wrong pasword]"
        });
        expect(response).to.have.status(401);
      } catch (error) {
        expect(error).to.be.ok;
      }
    });
  });
  context("change-password", function() {
    before(async function() {
      authorization_token = await authenticate(goodUser);
    });
    it(`should change a user password`, async function() {
      const response = await send(
        "post",
        routes.Auth.ChangePassword,
        authorization_token,
        {
          oldPassword: goodUser.password,
          newPassword: "newpassword"
        }
      );
      expect(response).to.have.status(204);
    });
  });
});
