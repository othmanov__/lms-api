import { getMockStudent, getFakeStudent } from "tests/units.utils";
import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { Student } from "@app/entity/Student";
import * as faker from "faker";
import "mocha";
import * as chai from "chai";
const expect = chai.expect;
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { getRepository } from "typeorm";
import { User } from "@app/entity/User";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import { authenticate } from "tests/e2e.utils";
const host = environment.api;
describe("routes#Students", function() {
  let authorization_token = null;
  let theServer: Server;
  const getMock = getFakeStudent;
  before(async () => {
    theServer = await createExpressApp();
    await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
  });
  after(async function() {
    await getRepository(Student).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  context("Best scenario", function() {
    it(`should list students`, async function() {
      const list = await Promise.all(
        [1, 2, 3].map(i => getMockStudent(getMock()))
      );

      await getRepository(Student).save(list);
      const response = await send("get", routes.Students, authorization_token);
      expect(response).to.have.status(200);
    });
    it(`should save student`, async function() {
      const mock = getMock();
      const response = await send(
        "post",
        routes.Students,
        authorization_token,
        mock
      );
      expect(response).to.have.status(201);
    });
    it(`should update an existing student`, async function() {
      const mock = getMock();
      const mockReplacement = getMock();
      const saved = await getMockStudent(mock);
      const response = await send(
        "patch",
        routes.Students,
        authorization_token,
        mockReplacement,
        saved.id
      );
      expect(response).to.have.status(204);
    });
    it(`should delete an existing student`, async function() {
      const mock = getMock();
      const saved = await getMockStudent(mock);
      const response = await send(
        "delete",
        routes.Students,
        authorization_token,
        null,
        saved.id
      );
      expect(response).to.have.status(204);
    });
  });
});
