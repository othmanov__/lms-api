import { getRepository, Not } from "typeorm";
import { routes } from "./../e2e.test";
import { Server } from "http";
import { createExpressApp, closeConnection } from "@app/index";
import { User, UserRoles } from "@app/entity/User";
import * as faker from "faker";
import "mocha";
import * as chai from "chai";
import chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
import environment from "@env";
import { Logger } from "@app/helpers/logger";
import { send } from "tests/e2e.test";
import { createUserOrDefault, getDefaultUserAuths } from "tests/units.test";
import { getMockUser } from "tests/units.utils";
import { authenticate } from "tests/e2e.utils";
const host = environment.api;
describe("routes#Users", function() {
  let authorization_token = null;
  let theServer: Server;
  const getMock = () => {
    return {
      username: faker.internet.userName(),
      email: faker.internet.exampleEmail(),
      password: faker.internet.password(),
      role: UserRoles.Admin
    } as User;
  };
  before(async () => {
    theServer = await createExpressApp();
    await createUserOrDefault();
    authorization_token = await authenticate(getDefaultUserAuths());
  });
  beforeEach(async () => {
    await getRepository(User).delete({
      username: Not("admin") /// all Users except the logged in one for api
    });
  });
  after(async function() {
    await getRepository(User).delete({});
    await closeConnection();
    theServer.close(() => {
      Logger.Infos("Server closed");
    });
  });
  describe("Best scenario", function() {
    it(`should list users`, async function() {
      const response = await send("get", routes.Users, authorization_token);
      chai.expect(response).to.have.status(200);
    });
    it(`should save user`, async function() {
      const mock = getMock();
      const response = await send(
        "post",
        routes.Users,
        authorization_token,
        mock
      );
      chai.expect(response).to.have.status(201);
    });
    it(`should update an existing user`, async function() {
      const mock = getMock();
      const mockReplacement = getMock();
      const saved = await getMockUser(mock);
      const response = await send(
        "patch",
        routes.Users,
        authorization_token,
        mockReplacement,
        saved.id
      );
      chai.expect(response).to.have.status(204);
    });
    it(`should delete an existing user`, async function() {
      const mock = getMock();
      const saved = await getMockUser(mock);
      const response = await send(
        "delete",
        routes.Users,
        authorization_token,
        null,
        saved.id
      );
      chai.expect(response).to.have.status(204);
    });
  });
});
