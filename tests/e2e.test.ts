import environment from "@env";
import * as request from "superagent";
import * as chai from "chai";
const should = chai.should();
import chaiHttp = require("chai-http");
chai.use(chaiHttp);
const host = environment.api;

export function send(
  type: "get" | "post" | "patch" | "delete" | "put" = "get",
  route: string,
  authorization_token: string,
  payload?: any,
  params?: any
) {
  return request
    .agent()
    [type](`${host}/${route}/${params ? params : ""}`)
    .set(
      authorization_token
        ? {
            Authorization: authorization_token
          }
        : { "Content-type": "application/json" }
    )
    .send(payload);
}
export const routes = {
  Auth: {
    Login: "auth/login",
    ChangePassword: "auth/change-password"
  },
  Users: "users",
  Students: "students",
  Teachers: "teachers",
  Lessons: "lessons",
  Groups: "groups",
  Posts: "posts",
  Courses: "courses",
  Enrolments: "enrolments",
  Units: "units",
  Grades: "grades",
  Annoncments: "announcements",
  Assignments: "assignments",
  Discussions: "discussions",
  Categories: "categories",
  Ressources: "uploads"
};
