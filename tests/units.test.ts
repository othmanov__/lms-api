import { getRepository } from "typeorm";
import { User } from "@app/entity/User";
import { Student } from "@app/entity/Student";

const auths = require("mocks/auth.json");
export async function createUserOrDefault(userData = auths) {
  const existant = await getRepository(User).findOne({
    username: userData.username
  });
  if (existant) {
    return existant;
  }
  let user = new User();
  user.username = userData.username;
  user.password = userData.password;
  user.role = userData.role;
  user.email = userData.email;
  await user.hashPassword();

  return await user.save();
}
export const getDefaultUserAuths = function() {
  return auths;
};
