import { routes, send } from "./e2e.test";

export function authenticate(goodUser: { username: string; password: string }) {
  return send("post", routes.Auth.Login, null, {
    username: goodUser.username,
    password: goodUser.password
  }).then(response => {
    return response.body.token;
  });
}
