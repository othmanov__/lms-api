import * as nodemailer from "nodemailer";
import environment from "@env";
import { Options } from "nodemailer/lib/mailer";
export class Mailer {
  private static singleton: Mailer;
  private transport;
  private sender;
  private constructor() {
    this.sender = environment.mailing.from || environment.mailing.user;
    this.transport = nodemailer.createTransport({
      // service: "gmail", // To activate be sure to check this page: https://nodemailer.com/usage/using-gmail/
      host: environment.mailing.host,
      port: Number(environment.mailing.port),
      auth: {
        user: environment.mailing.user,
        pass: environment.mailing.secret
      }
    });
  }
  static getMailer() {
    if (!Mailer.singleton) {
      Mailer.singleton = new Mailer();
    }
    return Mailer.singleton;
  }

  async sendMail(options: Partial<Options>) {
    const mailOptions: Options = {
      from: this.sender,
      ...options
    };
    await this.transport.sendMail(mailOptions);
  }
}
