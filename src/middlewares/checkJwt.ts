import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { getRepository } from "typeorm";
import { User } from "@app/entity/User";
import environment from "@env";

export const checkJwt = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = <string>req.headers["authorization"].split(" ")[1];
  let jwtPayload;
  try {
    jwtPayload = <any>jwt.verify(token, config.jwtSecret);
    res.locals.jwtPayload = jwtPayload;
  } catch (error) {
    res.status(401).send();
    return;
  }

  const { userId, username } = jwtPayload;
  res.locals.user = await getRepository(User).findOne(userId);
  const newToken = jwt.sign({ userId, username }, config.jwtSecret, {
    expiresIn: environment.tokenExpiresIn
  });
  res.setHeader("token", newToken);

  next();
};
