import environment from "@env";
import chalk from "chalk";
import * as fs from "fs";
import * as path from "path";
const log = console.log;

export class Logger {
  constructor() {
    log("instanciating Logger ");
  }
  static readonly errorLogFilename = "errors.log";
  static readonly infoLogFilename = "infos.log";
  static readonly warningLogFilename = "warnings.log";
  static readonly successLogFilename = "sucess.log";
  private static writeToFile(message,filename){
    try {
      const filePath = path.join(
        environment.logsDir,
        filename
      );
      if (!fs.existsSync(environment.logsDir)) {
        fs.mkdirSync(environment.logsDir, 0o722);
      }
      fs.writeFileSync(
        filePath,
        message + `\r`,
        {
          encoding: "utf8",
          flag: "a+"
        }
      );
    } catch (error) {
      console.warn(`\rSkipping file Logging :\r${error.message}`);
    }
  }
  public static Error(error: Error) {
    if (environment.logErrors) {
      const message = `⛔ [${error.name}]  ${error.message}`;
      log(chalk.red(message)); // Actually Logging the Error
      this.writeToFile(message,Logger.errorLogFilename)
    }
  }
  public static Warning(message) {
    environment.logWarnings && log(chalk.yellow("\r ⚠️ " + message));
  }
  public static Infos(message) {
    environment.logInfo && log(chalk.blue("\rℹ️ " + message));
  }
  public static Log(req,res) {
    const displayed = `@${chalk.whiteBright(new Date().toISOString())} [${chalk.cyan(req.method)}] ${chalk.green.bold(req.url)} (${chalk.yellow(res.statusCode)})`
    const message = `@${(new Date().toISOString())} [${(req.method)}] ${(req.url)} (${(res.statusCode)})`
     log(displayed);
     if (environment.logs) {
      this.writeToFile(message,Logger.infoLogFilename)
     }
  }
  public static Success(message) {
    environment.logs && log(chalk.green("\r✅ " + message));
  }
}
