import environment from "@env";
import * as path from "path";
const Pug = require("pug");
export class PugCompiler {
  private _templatesAbsolutePath;
  private static singleton: PugCompiler;
  private constructor() {
    this._templatesAbsolutePath = path.resolve(
      environment.baseDir,
      "templates"
    );
  }
  public static getCompiler() {
    if (!PugCompiler.singleton) {
      PugCompiler.singleton = new PugCompiler();
    }
    return PugCompiler.singleton;
  }
  public compile(filename: string, locals: any) {
    const templateFn = Pug.compileFile(
      path.join(this._templatesAbsolutePath, filename)
    );
    return templateFn(locals);
  }
}
