import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Announcement } from "../entity/Announcement";

class AnnouncementController {
  static listAll = async (req: Request, res: Response) => {
    const announcementRepository = getRepository(Announcement);
    const announcements = await announcementRepository.find();
    res.send(announcements);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const announcementRepository = getRepository(Announcement);
    try {
      const announcement = await announcementRepository.findOneOrFail(id);
      res.status(200).send(announcement);
    } catch (error) {
      res.status(404).send("Announcement not found");
    }
  };

  static newAnnouncement = async (req: Request, res: Response) => {
    let {
      title,
      contents,
      notes,
      tags,
      audience,
      importance
    } = req.body as Announcement;
    let announcement = new Announcement();
    announcement.title = title;
    announcement.contents = contents;
    announcement.notes = notes;
    announcement.tags = tags;
    announcement.audience = audience;
    announcement.importance = importance;

    const errors = await validate(announcement);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const announcementRepository = getRepository(Announcement);
    try {
      await announcementRepository.save(announcement);
    } catch (e) {
      res.status(409).send("Announcement not created");
      return;
    }
    res.status(201).send("Announcement created");
  };

  static editAnnouncement = async (req: Request, res: Response) => {
    const id = req.params.id;
    let {
      title,
      contents,
      notes,
      tags,
      audience,
      importance
    } = req.body as Announcement;
    const announcementRepository = getRepository(Announcement);
    let announcement;
    try {
      announcement = await announcementRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Announcement not found");
      return;
    }
    announcement.title = title;
    announcement.contents = contents;
    announcement.notes = notes;
    announcement.tags = tags;
    announcement.audience = audience;
    announcement.importance = importance;
    const errors = await validate(announcement);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await announcementRepository.save(announcement);
    } catch (e) {
      res.status(409).send("Announcement not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteAnnouncement = async (req: Request, res: Response) => {
    const id = req.params.id;
    const announcementRepository = getRepository(Announcement);
    let announcement: Announcement;
    try {
      announcement = await announcementRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Announcement not found");
      return;
    }
    announcementRepository.delete(id);
    res.status(204).send();
  };
}

export default AnnouncementController;
