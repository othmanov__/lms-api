import { validate } from "class-validator";
import { Request, Response } from "express";
import { Ressource } from "../entity/Ressource";
import { getRepository } from "typeorm";

class RessourceController {
  static listAll = async (req: Request, res: Response) => {
    const ressourceRepository = getRepository(Ressource);
    const ressources = await ressourceRepository.find();
    res.send(ressources);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const ressourceRepository = getRepository(Ressource);
    try {
      const ressource = await ressourceRepository.findOneOrFail(id);
      res.status(200).send(ressource);
    } catch (error) {
      res.status(404).send("Ressource not found");
    }
  };

  static newRessource = async (req: Request, res: Response) => {
    let { url, caption, type } = req.body as Ressource;
    let ressource = new Ressource();
    ressource.url = url;
    ressource.caption = caption;
    ressource.type = type;
    const errors = await validate(ressource);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const ressourceRepository = getRepository(Ressource);
    try {
      ressource = await ressourceRepository.save(ressource);
      ressource.url = await Ressource.updateUrl(
        ressource.url,
        ressource.type,
        ressource.id
      );
      await ressource.save();
    } catch (e) {
      res.status(409).send("Ressource not created");
      return;
    }
    res.status(201).send("Ressource created");
  };

  static editRessource = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { url, type, caption } = req.body as Ressource;
    const ressourceRepository = getRepository(Ressource);
    let ressource: Ressource;
    try {
      ressource = await ressourceRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Ressource not found");
      return;
    }
    ressource.url = await Ressource.updateUrl(url, type, ressource.id);
    ressource.caption = caption;
    ressource.type = type;
    const errors = await validate(ressource);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await ressourceRepository.save(ressource);
    } catch (e) {
      res.status(409).send("Ressource not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteRessource = async (req: Request, res: Response) => {
    const id = req.params.id;
    const ressourceRepository = getRepository(Ressource);
    let ressource: Ressource;
    try {
      ressource = await ressourceRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Ressource not found");
      return;
    }
    ressourceRepository.delete(id);
    res.status(204).send();
  };
}

export default RessourceController;
