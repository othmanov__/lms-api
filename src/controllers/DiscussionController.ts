import { Course } from "./../entity/Course";
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Discussion } from "../entity/Discussion";

class DiscussionController {
  static listAll = async (req: Request, res: Response) => {
    const discussionRepository = getRepository(Discussion);
    const discussions = await discussionRepository.find();
    res.send(discussions);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const discussionRepository = getRepository(Discussion);
    try {
      const discussion = await discussionRepository.findOneOrFail(id);
      res.status(200).send(discussion);
    } catch (error) {
      res.status(404).send("Discussion not found");
    }
  };

  static newDiscussion = async (req: Request, res: Response) => {
    let { category, topic, course, members } = req.body as Discussion;
    const courseId = req.params.courseId;
    let discussion = new Discussion();
    discussion.topic = topic;
    discussion.category = category;
    discussion.members = members;
    discussion.course = course
      ? course
      : courseId
      ? await getRepository(Course).findOne(courseId)
      : null;
    const errors = await validate(discussion);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const discussionRepository = getRepository(Discussion);
    try {
      await discussionRepository.save(discussion);
    } catch (e) {
      res.status(409).send("Discussion not created");
      return;
    }
    res.status(201).send("Discussion created");
  };

  static editDiscussion = async (req: Request, res: Response) => {
    const id = req.params.id;
    const courseId = req.params.courseId;
    let { category, topic, course, members } = req.body as Discussion;
    const discussionRepository = getRepository(Discussion);
    let discussion;
    try {
      discussion = await discussionRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Discussion not found");
      return;
    }
    discussion.topic = topic;
    discussion.category = category;
    discussion.members = members;
    discussion.course = course
      ? course
      : courseId
      ? await getRepository(Course).findOne(courseId)
      : null;
    const errors = await validate(discussion);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await discussionRepository.save(discussion);
    } catch (e) {
      res.status(409).send("Discussion not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteDiscussion = async (req: Request, res: Response) => {
    const id = req.params.id;
    const discussionRepository = getRepository(Discussion);
    let discussion: Discussion;
    try {
      discussion = await discussionRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Discussion not found");
      return;
    }
    discussionRepository.delete(id);
    res.status(204).send();
  };
}

export default DiscussionController;
