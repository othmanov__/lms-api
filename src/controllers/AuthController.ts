import { Request, Response } from "express";

import * as jwt from "jsonwebtoken";
import { addMilliseconds, getUnixTime } from "date-fns";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { User } from "../entity/User";
import config from "../config/config";
import environment from "@env";

class AuthController {
  static login = async (req: Request, res: Response) => {
    let { username, password } = req.body;
    if (!(username && password)) {
      res.status(400).send();
    }

    const userRepository = getRepository(User);
    let user: User;
    user = await userRepository.findOne({ where: { username: username } });
    if (!user) {
      res.status(401).send();
      return;
    }

    if (user && !(await user.checkIfUnencryptedPasswordIsValid(password))) {
      res.status(401).send();
      return;
    }
    const expiresIn: number = environment.tokenExpiresIn;
    const token = jwt.sign(
      { userId: user.id, username: user.username },
      config.jwtSecret,
      { expiresIn }
    );

    res.send({
      token,
      expiresIn: getUnixTime(addMilliseconds(new Date(), expiresIn))
    });
  };

  static changePassword = async (req: Request, res: Response) => {
    const id = res.locals.jwtPayload.userId;

    const { oldPassword, newPassword } = req.body;
    if (!(oldPassword && newPassword)) {
      res.status(400).send();
    }

    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (id) {
      return res.status(401).send();
    }

    if (!(await user.checkIfUnencryptedPasswordIsValid(oldPassword))) {
      res.status(401).send();
      return;
    }

    user.password = newPassword;
    const errors = await validate(user);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    await user.hashPassword();
    userRepository.save(user);

    res.status(204).send();
  };
}
export default AuthController;
