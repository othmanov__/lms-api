import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Course } from "../entity/Course";

class CourseController {
  static listAll = async (req: Request, res: Response) => {
    const courseRepository = getRepository(Course);
    const courses = await courseRepository.find();
    res.send(courses);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const courseRepository = getRepository(Course);
    try {
      const course = await courseRepository.findOneOrFail(id);
      res.status(200).send(course);
    } catch (error) {
      res.status(404).send("Course not found");
    }
  };

  static newCourse = async (req: Request, res: Response) => {
    let {
      name,
      description,
      startAt,
      endAt,
      password,
      subject,
      units
    } = req.body as Course;
    let course = new Course();
    course.name = name;
    course.startAt = startAt;
    course.endAt = endAt;
    course.password = password;
    course.description = description;
    course.subject = subject;
    course.units = units;
    if (course.password) {
      course.hashPassword();
    }
    const errors = await validate(course);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const courseRepository = getRepository(Course);
    try {
      await courseRepository.save(course);
    } catch (e) {
      res.status(500).send("Course not created");
      return;
    }
    res.status(201).send("Course created");
  };

  static editCourse = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { name, description, subject, units } = req.body as Course;
    const courseRepository = getRepository(Course);
    let course;
    try {
      course = await courseRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Course not found");
      return;
    }
    course.name = name;
    course.description = description;
    course.subject = subject;
    course.units = units;
    const errors = await validate(course);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await courseRepository.save(course);
    } catch (e) {
      res.status(409).send("Course not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteCourse = async (req: Request, res: Response) => {
    const id = req.params.id;
    const courseRepository = getRepository(Course);
    let course: Course;
    try {
      course = await courseRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Course not found");
      return;
    }
    courseRepository.delete(id);
    res.status(204).send();
  };
}

export default CourseController;
