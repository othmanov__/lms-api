import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Teacher } from "../entity/Teacher";

class TeacherController {
  static listAll = async (req: Request, res: Response) => {
    const teacherRepository = getRepository(Teacher);
    const teachers = await teacherRepository.find();

    res.send(teachers);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const teacherRepository = getRepository(Teacher);
    try {
      const teacher = await teacherRepository.findOneOrFail(id);
      res.status(200).send(teacher);
    } catch (error) {
      res.status(404).send("Teacher not found");
    }
  };

  static newTeacher = async (req: Request, res: Response) => {
    let {
      firstName,
      lastName,
      middleName,
      gov_number,
      dob
    } = req.body as Teacher;
    let teacher = new Teacher();
    teacher.firstName = firstName;
    teacher.lastName = lastName;
    teacher.middleName = middleName;
    teacher.gov_number = gov_number;
    teacher.dob = dob;

    const errors = await validate(teacher);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const teacherRepository = getRepository(Teacher);
    try {
      await teacherRepository.save(teacher);
    } catch (e) {
      res.status(409).send("nsc already in use");
      return;
    }
    res.status(201).send("Teacher created");
  };

  static editTeacher = async (req: Request, res: Response) => {
    const id = req.params.id;
    let {
      firstName,
      lastName,
      middleName,
      gov_number,
      dob
    } = req.body as Teacher;

    const teacherRepository = getRepository(Teacher);
    let teacher: Teacher;
    try {
      teacher = await teacherRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Teacher not found");
      return;
    }
    teacher.firstName = firstName;
    teacher.lastName = lastName;
    teacher.middleName = middleName;
    teacher.gov_number = gov_number;
    teacher.dob = dob;
    const errors = await validate(teacher);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await teacherRepository.save(teacher);
    } catch (e) {
      res.status(409).send("nsc already in use");
      return;
    }

    res.status(204).send();
  };

  static deleteTeacher = async (req: Request, res: Response) => {
    const id = req.params.id;
    const teacherRepository = getRepository(Teacher);
    let teacher: Teacher;
    try {
      teacher = await teacherRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Teacher not found");
      return;
    }
    teacherRepository.delete(id);

    res.status(204).send();
  };
}

export default TeacherController;
