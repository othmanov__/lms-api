import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Student } from "../entity/Student";

class StudentController {
  static listAll = async (req: Request, res: Response) => {
    const studentRepository = getRepository(Student);
    const students = await studentRepository.find();

    res.send(students);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const studentRepository = getRepository(Student);
    try {
      const student = await studentRepository.findOneOrFail(id);
      res.status(200).send(student);
    } catch (error) {
      res.status(404).send("Student not found");
    }
  };

  static newStudent = async (req: Request, res: Response) => {
    let { firstName, lastName, middleName, nsc, dob } = req.body;
    let student = new Student();
    student.firstName = firstName;
    student.lastName = lastName;
    student.middleName = middleName;
    student.nsc = nsc;
    student.dob = dob;

    const errors = await validate(student);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const studentRepository = getRepository(Student);
    try {
      await studentRepository.save(student);
    } catch (e) {
      res.status(409).send(e);
      return;
    }
    res.status(201).send("Student created");
  };

  static editStudent = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { firstName, lastName, middleName, nsc, dob } = req.body;

    const studentRepository = getRepository(Student);
    let student;
    try {
      student = await studentRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Student not found");
      return;
    }
    student.firstName = firstName;
    student.lastName = lastName;
    student.middleName = middleName;
    student.nsc = nsc;
    student.dob = dob;
    const errors = await validate(student);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await studentRepository.save(student);
    } catch (e) {
      res.status(409).send("nsc already in use");
      return;
    }

    res.status(204).send();
  };

  static deleteStudent = async (req: Request, res: Response) => {
    const id = req.params.id;
    const studentRepository = getRepository(Student);
    let student: Student;
    try {
      student = await studentRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Student not found");
      return;
    }
    studentRepository.delete(id);

    res.status(204).send();
  };
}

export default StudentController;
