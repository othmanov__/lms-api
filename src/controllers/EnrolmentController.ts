import { User } from "@app/entity/User";
import { Student } from "./../entity/Student";
import { Course } from "./../entity/Course";
import { Request, Response } from "express";
import { getRepository, Equal } from "typeorm";
import { validate, ValidationError } from "class-validator";
import { Enrolment } from "../entity/Enrolment";

class EnrolmentController {
  static listAll = async (req: Request, res: Response) => {
    const enrolmentRepository = getRepository(Enrolment);
    const enrolments = await enrolmentRepository.find();
    res.send(enrolments);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const enrolmentRepository = getRepository(Enrolment);
    try {
      const enrolment = await enrolmentRepository.findOneOrFail(id);
      res.status(200).send(enrolment);
    } catch (error) {
      res.status(404).send("Enrolment not found");
    }
  };

  static newEnrolment = async (req: Request, res: Response) => {
    let { student, isPaid, course, expirationDate } = req.body as Enrolment;
    const courseId = req.params.courseId;
    course = course || (await getRepository(Course).findOne(courseId));
    const loggedInStudent = await getRepository(Student).findOne({
      profile: res.locals.user
    });
    let enrolment = new Enrolment();
    enrolment.isPaid = isPaid;
    enrolment.student = student || loggedInStudent;
    enrolment.expirationDate = expirationDate;
    enrolment.course = course;

    const errors = await validate(enrolment);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const enrolmentRepository = getRepository(Enrolment);
    try {
      await enrolmentRepository.save(enrolment);
    } catch (e) {
      res.status(409).send("Enrolment not created");
      return;
    }
    res.status(201).send("Enrolment created");
  };

  static editEnrolment = async (req: Request, res: Response) => {
    const id = req.params.id;
    const courseId = req.params.courseId;
    let {
      student,
      isPaid,
      isExpired,
      course,
      expirationDate
    } = req.body as Enrolment;
    course = course || (await getRepository(Course).findOne(courseId));
    const loggedInStudent = await getRepository(Student).findOne({
      profile: res.locals.user
    });
    student = student || loggedInStudent;
    const enrolmentRepository = getRepository(Enrolment);
    let enrolment;
    try {
      enrolment = await enrolmentRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Enrolment not found");
      return;
    }
    enrolment.isExpired = isExpired;
    enrolment.isPaid = isPaid;
    enrolment.student = student;
    enrolment.expirationDate = expirationDate;
    enrolment.course = course;
    const errors = await validate(enrolment);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await enrolmentRepository.save(enrolment);
    } catch (e) {
      res.status(409).send("Enrolment not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteEnrolment = async (req: Request, res: Response) => {
    const id = req.params.id;
    const enrolmentRepository = getRepository(Enrolment);
    let enrolment: Enrolment;
    try {
      enrolment = await enrolmentRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Enrolment not found");
      return;
    }
    enrolmentRepository.delete(id);
    res.status(204).send();
  };
}

export default EnrolmentController;
