import { Teacher } from "@app/entity/Teacher";
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Group } from "../entity/Group";

class GroupController {
  static listAll = async (req: Request, res: Response) => {
    const groupRepository = getRepository(Group);
    const groups = await groupRepository.find();
    res.send(groups);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const groupRepository = getRepository(Group);
    try {
      const group = await groupRepository.findOneOrFail(id);
      res.status(200).send(group);
    } catch (error) {
      res.status(404).send("Group not found");
    }
  };

  static newGroup = async (req: Request, res: Response) => {
    let { name, members, admin, closed, description } = req.body as Group;
    let group = new Group();
    group.name = name;
    group.description = description;
    group.closed = closed;
    group.admin = admin;
    group.members = members;
    const errors = await validate(group);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const groupRepository = getRepository(Group);
    try {
      await groupRepository.save(group);
    } catch (e) {
      res.status(409).send("Group not created");
      return;
    }
    res.status(201).send("Group created");
  };

  static editGroup = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { name, members, admin, closed, description } = req.body as Group;
    const groupRepository = getRepository(Group);
    let group;
    try {
      group = await groupRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Group not found");
      return;
    }
    group.name = name;
    group.description = description;
    group.closed = closed;
    group.admin = admin;
    group.members = members;
    const errors = await validate(group);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await groupRepository.save(group);
    } catch (e) {
      res.status(409).send("Group not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteGroup = async (req: Request, res: Response) => {
    const id = req.params.id;
    const groupRepository = getRepository(Group);
    let group: Group;
    try {
      group = await groupRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Group not found");
      return;
    }
    groupRepository.delete(id);
    res.status(204).send();
  };
}

export default GroupController;
