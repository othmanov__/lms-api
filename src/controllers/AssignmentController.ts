import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Assignment } from "../entity/Assignment";
import { Course } from "@app/entity/Course";

class AssignmentController {
  static listAll = async (req: Request, res: Response) => {
    const assignmentRepository = getRepository(Assignment);
    const assignments = await assignmentRepository.find();
    res.send(assignments);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const assignmentRepository = getRepository(Assignment);
    try {
      const assignment = await assignmentRepository.findOneOrFail(id);
      res.status(200).send(assignment);
    } catch (error) {
      res.status(404).send("Assignment not found");
    }
  };

  static newAssignment = async (req: Request, res: Response) => {
    let {
      title,
      instructions,
      startAt,
      endAt,
      acceptUntil,
      type,
      course
    } = req.body as Assignment;
    const courseId = req.params.courseId;
    let assignment = new Assignment();
    assignment.title = title;
    assignment.instructions = instructions;
    assignment.startAt = startAt;
    assignment.endAt = endAt;
    assignment.acceptUntil = acceptUntil;
    assignment.type = type;
    if (courseId) {
      course = await getRepository(Course).findOne(courseId);
    }
    assignment.course = course;
    const errors = await validate(assignment);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const assignmentRepository = getRepository(Assignment);
    try {
      await assignmentRepository.save(assignment);
    } catch (e) {
      res.status(409).send("Assignment not created");
      return;
    }
    res.status(201).send("Assignment created");
  };

  static editAssignment = async (req: Request, res: Response) => {
    const id = req.params.id;
    let {
      title,
      instructions,
      startAt,
      endAt,
      acceptUntil,
      type,
      course
    } = req.body as Assignment;
    const assignmentRepository = getRepository(Assignment);
    const courseId = req.params.courseId;
    let assignment;
    try {
      assignment = await assignmentRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Assignment not found");
      return;
    }
    assignment.title = title;
    assignment.instructions = instructions;
    assignment.startAt = startAt;
    assignment.endAt = endAt;
    assignment.acceptUntil = acceptUntil;
    assignment.type = type;
    if (courseId) {
      course = await getRepository(Course).findOne(courseId);
    }
    assignment.course = course;
    const errors = await validate(assignment);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await assignmentRepository.save(assignment);
    } catch (e) {
      res.status(409).send("Assignment not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteAssignment = async (req: Request, res: Response) => {
    const id = req.params.id;
    const assignmentRepository = getRepository(Assignment);
    let assignment: Assignment;
    try {
      assignment = await assignmentRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Assignment not found");
      return;
    }
    assignmentRepository.delete(id);
    res.status(204).send();
  };
}

export default AssignmentController;
