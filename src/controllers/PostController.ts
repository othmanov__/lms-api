import { Course } from "./../entity/Course";
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Post } from "../entity/Post";

class PostController {
  static listAll = async (req: Request, res: Response) => {
    const postRepository = getRepository(Post);
    const posts = await postRepository.find();
    res.send(posts);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const postRepository = getRepository(Post);
    try {
      const post = await postRepository.findOneOrFail(id);
      res.status(200).send(post);
    } catch (error) {
      res.status(404).send("Post not found");
    }
  };

  static newPost = async (req: Request, res: Response) => {
    let { title, content, categories, medias } = req.body as Post;
    let post = new Post();
    post.title = title;
    post.content = content;
    post.categories = categories;
    post.medias = medias;
    post.createdBy = res.locals.user;
    const errors = await validate(post);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const postRepository = getRepository(Post);
    try {
      await postRepository.save(post);
    } catch (e) {
      res.status(409).send("Post not created");
      return;
    }
    res.status(201).send("Post created");
  };

  static editPost = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { title, content, categories, medias } = req.body as Post;
    const postRepository = getRepository(Post);
    let post;
    try {
      post = await postRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Post not found");
      return;
    }
    post.title = title;
    post.content = content;
    post.categories = categories;
    post.medias = medias;
    const errors = await validate(post);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await postRepository.save(post);
    } catch (e) {
      res.status(409).send("Post not updated");
      return;
    }
    res.status(204).send();
  };

  static deletePost = async (req: Request, res: Response) => {
    const id = req.params.id;
    const postRepository = getRepository(Post);
    let post: Post;
    try {
      post = await postRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Post not found");
      return;
    }
    postRepository.delete(id);
    res.status(204).send();
  };
}

export default PostController;
