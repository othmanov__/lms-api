import { Category } from "./../entity/Category";
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

class CategoryController {
  static listAll = async (req: Request, res: Response) => {
    const announcementRepository = getRepository(Category);
    const announcements = await announcementRepository.find();
    res.send(announcements);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const announcementRepository = getRepository(Category);
    try {
      const category = await announcementRepository.findOneOrFail(id);
      res.status(200).send(category);
    } catch (error) {
      res.status(404).send("Category not found");
    }
  };

  static newCategory = async (req: Request, res: Response) => {
    let { name, image, description } = req.body as Category;
    let category = new Category();
    category.name = name;
    category.description = description;
    category.image = image;

    const errors = await validate(category);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const announcementRepository = getRepository(Category);
    try {
      await announcementRepository.save(category);
    } catch (e) {
      res.status(409).send("Category not created");
      return;
    }
    res.status(201).send("Category created");
  };

  static editCategory = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { name, description, image } = req.body as Category;
    const announcementRepository = getRepository(Category);
    let category;
    try {
      category = await announcementRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Category not found");
      return;
    }
    category.name = name;
    category.description = description;
    category.image = image;
    const errors = await validate(category);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await announcementRepository.save(category);
    } catch (e) {
      res.status(409).send("Category not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteCategory = async (req: Request, res: Response) => {
    const id = req.params.id;
    const announcementRepository = getRepository(Category);
    let category: Category;
    try {
      category = await announcementRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Category not found");
      return;
    }
    announcementRepository.delete(id);
    res.status(204).send();
  };
}

export default CategoryController;
