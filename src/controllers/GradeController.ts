import { Teacher } from "@app/entity/Teacher";
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Grade } from "../entity/Grade";
import { User } from "@app/entity/User";

class GradeController {
  static listAll = async (req: Request, res: Response) => {
    const gradeRepository = getRepository(Grade);
    const grades = await gradeRepository.find();
    res.send(grades);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const gradeRepository = getRepository(Grade);
    try {
      const grade = await gradeRepository.findOneOrFail(id);
      res.status(200).send(grade);
    } catch (error) {
      res.status(404).send("Grade not found");
    }
  };

  static newGrade = async (req: Request, res: Response) => {
    let { value, comments, student, assignment } = req.body as Grade;
    let teacher;
    try {
      const user = await getRepository(User).findOneOrFail(
        res.locals.jwtPayload.userId
      );
      teacher = await getRepository(Teacher).findOneOrFail({
        profile: user
      });
    } catch (error) {
      return res.status(409).send("Grade not created, user not found");
    }
    let grade = new Grade();
    grade.value = value;
    grade.signedBy = teacher;
    grade.student = student;
    grade.assignment = assignment;
    grade.comments = comments;
    const errors = await validate(grade);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const gradeRepository = getRepository(Grade);
    try {
      await gradeRepository.save(grade);
    } catch (e) {
      res.status(409).send("Grade not created");
      return;
    }
    res.status(201).send("Grade created");
  };

  static editGrade = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { itemName, itemId, value, comments, dueDate } = req.body;
    const gradeRepository = getRepository(Grade);
    let grade;
    try {
      grade = await gradeRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Grade not found");
      return;
    }
    grade.itemId = itemId;
    grade.itemName = itemName;
    grade.value = value;
    grade.dueDate = dueDate;
    grade.comments = comments;
    const errors = await validate(grade);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await gradeRepository.save(grade);
    } catch (e) {
      res.status(409).send("Grade not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteGrade = async (req: Request, res: Response) => {
    const id = req.params.id;
    const gradeRepository = getRepository(Grade);
    let grade: Grade;
    try {
      grade = await gradeRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Grade not found");
      return;
    }
    gradeRepository.delete(id);
    res.status(204).send();
  };
}

export default GradeController;
