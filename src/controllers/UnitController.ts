import { Course } from "./../entity/Course";
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Unit } from "../entity/Unit";

class UnitController {
  static listAll = async (req: Request, res: Response) => {
    const unitRepository = getRepository(Unit);
    const units = await unitRepository.find();
    res.send(units);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const unitRepository = getRepository(Unit);
    try {
      const unit = await unitRepository.findOneOrFail(id);
      res.status(200).send(unit);
    } catch (error) {
      res.status(404).send("Unit not found");
    }
  };

  static newUnit = async (req: Request, res: Response) => {
    let { name, course, lessons } = req.body as Unit;
    const courseId = req.params.courseId;
    let unit = new Unit();
    unit.name = name;
    unit.course = course
      ? course
      : courseId
      ? await getRepository(Course).findOne(courseId)
      : null;
    unit.lessons = lessons;
    const errors = await validate(unit);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const unitRepository = getRepository(Unit);
    try {
      await unitRepository.save(unit);
    } catch (e) {
      res.status(409).send("Unit not created");
      return;
    }
    res.status(201).send("Unit created");
  };

  static editUnit = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { name, course, lessons } = req.body as Unit;
    const courseId = req.params.courseId;
    const unitRepository = getRepository(Unit);
    let unit;
    try {
      unit = await unitRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Unit not found");
      return;
    }
    unit.name = name;
    unit.course = course
      ? course
      : courseId
      ? await getRepository(Course).findOne(courseId)
      : null;
    unit.lessons = lessons;
    const errors = await validate(unit);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await unitRepository.save(unit);
    } catch (e) {
      res.status(409).send("Unit not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteUnit = async (req: Request, res: Response) => {
    const id = req.params.id;
    const unitRepository = getRepository(Unit);
    let unit: Unit;
    try {
      unit = await unitRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Unit not found");
      return;
    }
    unitRepository.delete(id);
    res.status(204).send();
  };
}

export default UnitController;
