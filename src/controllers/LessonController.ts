import { validate } from "class-validator";
import { Request, Response } from "express";
import { Lesson } from "../entity/Lesson";
import { getRepository } from "typeorm";

class LessonController {
  static listAll = async (req: Request, res: Response) => {
    const lessonRepository = getRepository(Lesson);
    const lessons = await lessonRepository.find();
    res.send(lessons);
  };

  static getOneById = async (req: Request, res: Response) => {
    const id: string = req.params.id;

    const lessonRepository = getRepository(Lesson);
    try {
      const lesson = await lessonRepository.findOneOrFail(id);
      res.status(200).send(lesson);
    } catch (error) {
      res.status(404).send("Lesson not found");
    }
  };

  static newLesson = async (req: Request, res: Response) => {
    let { title, content, duration, ressources } = req.body as Lesson;
    let lesson = new Lesson();
    lesson.title = title;
    lesson.content = content;
    lesson.duration = duration;
    lesson.ressources = ressources;
    const errors = await validate(lesson);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    const lessonRepository = getRepository(Lesson);
    try {
      await lessonRepository.save(lesson);
    } catch (e) {
      res.status(409).send("Lesson not created");
      return;
    }
    res.status(201).send("Lesson created");
  };

  static editLesson = async (req: Request, res: Response) => {
    const id = req.params.id;
    let { title, content, duration, ressources } = req.body as Lesson;
    const lessonRepository = getRepository(Lesson);
    let lesson;
    try {
      lesson = await lessonRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Lesson not found");
      return;
    }
    lesson.title = title;
    lesson.content = content;
    lesson.duration = duration;
    lesson.ressources = ressources;
    const errors = await validate(lesson);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    try {
      await lessonRepository.save(lesson);
    } catch (e) {
      res.status(409).send("Lesson not updated");
      return;
    }
    res.status(204).send();
  };

  static deleteLesson = async (req: Request, res: Response) => {
    const id = req.params.id;
    const lessonRepository = getRepository(Lesson);
    let lesson: Lesson;
    try {
      lesson = await lessonRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Lesson not found");
      return;
    }
    lessonRepository.delete(id);
    res.status(204).send();
  };
}

export default LessonController;
