import { Course } from "./Course";
import { Lesson } from "./Lesson";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  BaseEntity,
  ManyToMany,
  JoinTable
} from "typeorm";
@Entity()
export class Unit extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 300 })
  name: string;

  @ManyToMany(type => Lesson, { nullable: true, eager: true, cascade: true })
  @JoinTable()
  lessons: Lesson[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @ManyToOne(
    type => Course,
    course => course.units,
    { eager: true, onDelete: "SET NULL", onUpdate: "CASCADE" }
  )
  @JoinColumn()
  course: Course;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
