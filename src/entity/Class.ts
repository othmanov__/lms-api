import { Teacher } from "./Teacher";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  BaseEntity
} from "typeorm";
import { Student } from "./Student";
@Entity()
export class Class extends BaseEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @ManyToMany(type => Student, {
    eager: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    nullable: true
  })
  @JoinTable()
  students?: Student[];

  @ManyToMany(type => Teacher, {
    eager: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    nullable: true
  })
  @JoinTable()
  teachers?: Teacher[];

  @Column({ length: 300 })
  name: string;

  @Column({ length: 20 })
  code: string;

  @Column()
  @CreateDateColumn()
  createdAt?: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt?: Date;
}
