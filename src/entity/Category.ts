import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  JoinColumn,
  OneToOne
} from "typeorm";
import { Ressource } from "@app/entity/Ressource";
@Entity()
export class Category extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 300, default: "Uncategorized" })
  name: string;

  @Column()
  description: string;
  @OneToOne(type => Ressource, {
    nullable: true,
    onUpdate: "CASCADE",
    onDelete: "SET NULL",
    cascade: true,
    eager: true
  })
  @JoinColumn()
  image: Ressource;
  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
