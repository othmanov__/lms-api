import { Ressource } from "./Ressource";
import { ProfileDetails } from "./ProfileDetails";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  BaseEntity,
  BeforeInsert,
  AfterInsert,
  BeforeUpdate
} from "typeorm";
import { IsNotEmpty, IsEmail, validateOrReject } from "class-validator";
import * as bcrypt from "bcryptjs";
import { Mailer } from "@app/middlewares/mailer";
import { PugCompiler } from "@app/helpers/pug-compiler";
import environment from "@env";

export enum UserRoles {
  Student = "student",
  Teacher = "teacher",
  Moderator = "moderator",
  Admin = "admin"
}
@Entity()
export class User extends BaseEntity {
  @BeforeInsert()
  @BeforeUpdate()
  async validate() {
    await validateOrReject(this);
  }
  @PrimaryGeneratedColumn()
  id: number;
  @OneToOne(type => Ressource, {
    nullable: true,
    onUpdate: "CASCADE",
    onDelete: "CASCADE",
    cascade: true
  })
  avatar: Ressource;

  @Column({ length: 150 })
  @IsNotEmpty()
  username: string;

  @Column({ type: "text" })
  @IsNotEmpty()
  password: string;

  @Column()
  role: string;

  @Column({ default: false })
  isAdmin: boolean;

  @Column({ default: false })
  isStaff: boolean;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @Column({ unique: true })
  @IsEmail()
  email: string;

  @Column({ unique: true, nullable: true })
  hash: string;

  @Column({ nullable: true })
  email_verified_at?: Date;

  @OneToOne(
    type => ProfileDetails,
    details => details.user,
    {
      nullable: true,
      eager: true
    }
  )
  profile: ProfileDetails;
  async hashPassword() {
    return new Promise(async (resolve, reject) => {
      try {
        this.password = await bcrypt.hash(this.password, 8);
        resolve(this.password);
      } catch (error) {
        reject(error);
      }
    });
  }

  async checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compare(unencryptedPassword, this.password);
  }
  @BeforeUpdate()
  async notifyPasswordChange() {
    if (!environment.emails) {
      return;
    }
    const locals = {
      hostname: process.env.HOSTNAME,
      username: this.username,
      email: this.email
    };

    const subject = `Dear ${this.username}. Your password has been changed`;
    const html = PugCompiler.getCompiler().compile(
      "password_changed.pug",
      locals
    );
    await Mailer.getMailer().sendMail({ to: [this.email], subject, html });
  }
  @AfterInsert()
  async userAfterInsert() {
    await this.setRandomHash();
    if (!environment.emails) {
      return;
    }
    // Emails comes here
    await this.sendVerificationEmail();
  }
  async setRandomHash() {
    this.hash = await bcrypt.genSalt();
  }
  async sendVerificationEmail() {
    if (!environment.emails) {
      return;
    }
    if (!this.email) {
      throw new Error(
        "User doesn't have an email! aborting email verification."
      );
    }
    if (!this.hash) {
      throw new Error("User Hash Is Invalid ! aborting email verification.");
    }

    const subject = `Dear ${this.username}. Please verify your email`;
    const locals = {
      hostname: process.env.HOSTNAME,
      username: this.username,
      email: this.email,
      hash: this.hash
    };

    const html = PugCompiler.getCompiler().compile(
      "validate_email.pug",
      locals
    );
    await Mailer.getMailer().sendMail({
      to: [this.email],
      subject,
      html
    });
  }
}
