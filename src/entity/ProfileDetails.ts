import { IsEmail, Matches } from "class-validator";
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  BaseEntity,
  OneToOne,
  JoinColumn
} from "typeorm";
import { User } from "./User";

@Entity()
export class ProfileDetails extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, type: "text" })
  bio?: string;

  @Column({ nullable: true })
  @Matches(/\d{9,15}/)
  phone1?: string;

  @Column({ nullable: true })
  @Matches(/\d{9,15}/)
  phone2?: string;

  @Column({ length: 255, nullable: true })
  facebook?: string;

  @Column({ length: 255, nullable: true })
  google?: string;

  @Column({ length: 255, nullable: true })
  outlook?: string;

  @Column()
  @CreateDateColumn()
  createdAt?: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt?: Date;

  @OneToOne(
    type => User,
    user => user.profile,
    {
      cascade: true,
      onDelete: "CASCADE",
      onUpdate: "CASCADE"
    }
  )
  @JoinColumn()
  user: User;
}
