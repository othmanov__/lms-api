export enum AudienceType {
  Students = "Students",
  Instructors = "Instructors",
  Admins = "Admins",
  Public = "Public"
}
export enum ImportanceEnum {
  Normal,
  Medium,
  High
}

export enum RessourceTypeEnum {
  DEFAULT = "application/octet-stream",
  MD = "text/markdown",
  PDF = "application/pdf",
  DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  ODT = "application/vnd.oasis.opendocument.text",
  PPTX = "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  XLS = "application/vnd.ms-excel",
  XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  CSV = "text/csv",
  JPG = "image/jpg",
  PNG = "image/png",
  ZIP = "application/zip",
  MP3 = "audio/mp3",
  OGG = "audio/ogg"
}
export enum AssignmentTypeEnum {
  Test = "test",
  FinalTest = "final test",
  QCM = "QCM",
  EntryTest = "Entry Test",
  Assignment = "Assignment"
}
