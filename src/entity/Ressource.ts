import { RessourceTypeEnum } from "./enums";
import environment from "@env";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  AfterRemove
} from "typeorm";
import { validateOrReject, Matches, ValidateIf } from "class-validator";
import * as fs from "fs";
import * as path from "path";
import * as config from "config";
import * as mimetypes from "mime-types";
import { deleteFile, deleteFileAsync } from "utils";
@Entity()
export class Ressource extends BaseEntity {
  static BASE_URL = path.resolve(environment.mediaBase);
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  type: string;
  @Column()
  @ValidateIf(
    ressource =>
      config.acceptedUploadExtensions.includes(
        mimetypes.lookup(ressource.url) as RessourceTypeEnum
      ),
    {
      message: `$value is not a valid document`
    }
  )
  url: string;
  @Column({ length: 500, nullable: true })
  caption: string;
  @Column()
  @CreateDateColumn()
  createdAt: Date;
  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
  @AfterRemove()
  async deleteRessource() {
    await deleteFileAsync(this.url);
  }
  @BeforeInsert()
  @BeforeUpdate()
  async validate() {
    await validateOrReject(this);
  }

  public static async updateUrl(
    tempUrl: string,
    type: string,
    id: any,
    rmTempFile: boolean = environment.type !== "test"
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      if (!fs.existsSync(Ressource.BASE_URL)) {
        fs.mkdirSync(Ressource.BASE_URL);
      }
      fs.exists(tempUrl, exists => {
        if (exists) {
          // TODO: use external file location resolver for different FS stuctures (flat/tree)
          const targetUrl = path.join(
            Ressource.BASE_URL,
            `${id.toString() + "_" + Date.now()}.${mimetypes.extension(type)}`
          );
          fs.copyFile(tempUrl, targetUrl, err => {
            if (err) {
              return reject(err);
            }
            if (rmTempFile) {
              try {
                fs.unlinkSync(tempUrl);
                resolve(targetUrl);
              } catch (error) {
                reject(error);
              }
            } else {
              resolve(targetUrl);
            }
          });
        } else {
          reject(new Error("Temp file " + tempUrl + " Not found"));
        }
      });
    });
  }
}
