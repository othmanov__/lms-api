import { Ressource } from "./Ressource";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  BaseEntity
} from "typeorm";
@Entity()
export class Lesson extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 300 })
  title: string;
  @Column()
  content: string;
  @ManyToMany(type => Ressource)
  @JoinTable()
  ressources: Ressource[];
  @Column()
  duration: number;
  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
