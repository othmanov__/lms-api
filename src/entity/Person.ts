import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
  BaseEntity,
  Entity,
  TableInheritance,
  AfterRemove,
  getRepository
} from "typeorm";
import { User } from "./User";
@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })
export abstract class Person extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  middleName: string;

  @Column()
  dob: Date;

  @OneToOne(type => User, {
    nullable: true,
    cascade: true,
    onUpdate: "CASCADE",
    onDelete: "NO ACTION",
    eager: true
  })
  @JoinColumn()
  profile: User;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  public toString() {
    return `${this.firstName} ${this.lastName}`;
  }
  // @AfterRemove()
  // async deleteRelatedUserAccount() {
  //   if (!this.profile) {
  //     return; // No profile linked to the student
  //   }
  //   await getRepository(User).delete({
  //     id: this.profile.id
  //   });
  // }
}
