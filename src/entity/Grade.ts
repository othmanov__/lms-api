import { User } from "@app/entity/User";
import { Teacher } from "@app/entity/Teacher";
import { Assignment } from "./Assignment";
import { Student } from "./Student";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  BaseEntity,
  JoinColumn,
  OneToOne
} from "typeorm";
import { IsInt, Max, Min } from "class-validator";
@Entity()
export class Grade extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Student, { nullable: true })
  @JoinColumn()
  student: Student;
  @OneToOne(type => Teacher)
  @JoinColumn()
  signedBy: Teacher;

  @OneToOne(type => Assignment)
  @JoinColumn()
  assignment: Assignment;

  @Column()
  @IsInt()
  @Max(300)
  @Min(0)
  value: number;
  @Column()
  comments: string;
  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
