import { User } from "./User";
import { Course } from "./Course";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
  OneToOne,
  BaseEntity,
  JoinColumn
} from "typeorm";
import { Category } from "./Category";
@Entity()
export class Discussion extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 300 })
  topic: string;

  @OneToOne(type => Category, {
    nullable: true,
    eager: true,
    onUpdate: "CASCADE",
    onDelete: "SET NULL"
  })
  @JoinColumn()
  category: Category;

  @ManyToOne(
    type => Course,
    course => course.discussions,
    { nullable: true, eager: true, onUpdate: "CASCADE", onDelete: "SET NULL" }
  )
  @JoinColumn()
  course: Course;

  @ManyToMany(type => User, {
    eager: true,
    onUpdate: "CASCADE",
    onDelete: "CASCADE"
  })
  @JoinTable()
  members: User[];

  @Column({ default: false })
  closed: boolean;
  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
