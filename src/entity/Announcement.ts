import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  BaseEntity
} from "typeorm";
import { MaxLength, MinLength } from "class-validator";
import { AudienceType, ImportanceEnum } from "./enums";
@Entity()
export class Announcement extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 500 })
  title: string;

  @Column()
  @MinLength(10)
  contents: string;

  @Column({ nullable: true })
  @MaxLength(3000)
  notes: string;

  @Column("simple-array")
  tags: string[];

  @Column({
    type: "varchar",
    default: AudienceType.Public
  })
  audience: AudienceType;

  @Column({
    type: "varchar",
    default: ImportanceEnum.Normal
  })
  importance: ImportanceEnum;
  @CreateDateColumn()
  createdAt: Date;
}
