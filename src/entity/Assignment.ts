import { AssignmentTypeEnum } from "./enums";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  BaseEntity
} from "typeorm";
import { Course } from "./Course";
import { Validate } from "class-validator";
export const AssignmentTypes = [
  AssignmentTypeEnum.Assignment,
  AssignmentTypeEnum.EntryTest,
  AssignmentTypeEnum.FinalTest,
  AssignmentTypeEnum.QCM,
  AssignmentTypeEnum.Test
];
@Entity()
export class Assignment extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 300 })
  title: string;

  @ManyToOne(type => Course, { eager: true })
  course: Course;
  @Column()
  startAt: Date;
  @Column()
  endAt: Date;

  @Column({ nullable: true })
  acceptUntil: Date;
  @Column({ nullable: true, type: "varchar" })
  @Validate(value => AssignmentTypes.indexOf(value) != -1)
  type: string;
  @Column()
  instructions: string;
  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
