import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn
} from "typeorm";
import { Course } from "./Course";
import { Student } from "./Student";
@Unique("uniqueCourseStudentEnrolement", ["course", "student"])
@Entity()
export class Enrolment extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(
    type => Student,
    student => student.enrolment,
    { eager: true, onDelete: "NO ACTION", onUpdate: "CASCADE", nullable: false }
  )
  @JoinColumn()
  student: Student;
  @ManyToOne(type => Course, {
    eager: true,
    onDelete: "NO ACTION",
    onUpdate: "CASCADE",
    nullable: false
  })
  @JoinColumn()
  course: Course;
  @UpdateDateColumn()
  enroledlDate: Date;
  @Column({ default: false })
  isPaid: boolean;
  @Column({ nullable: true })
  isExpired: boolean;
  @Column({ nullable: true })
  expirationDate: Date;
}
