import { Person } from "./Person";
import {
  Column,
  OneToMany,
  AfterInsert,
  AfterRemove,
  getRepository,
  ChildEntity,
  JoinColumn
} from "typeorm";
import { Length, Matches } from "class-validator";
import { Enrolment } from "@app/entity/Enrolment";
import { User, UserRoles } from "./User";
import environment from "@env";
@ChildEntity()
export class Student extends Person {
  @Column({ unique: true, nullable: false })
  @Matches(/\d{10}/, {
    message: "$value is not a valid national student code"
  })
  nsc: string;
  @OneToMany(
    type => Enrolment,
    enrolment => enrolment.student,
    {
      nullable: true
    }
  )
  enrolment: Enrolment;
  // @AfterRemove()
  // async removeUser() {
  //   if (!this.profile) {
  //     return; // Already removed
  //   }
  //   await this.profile.remove();
  // }
  @AfterInsert()
  async createStudentUser() {
    if (!this.nsc) {
      throw new Error("Student needs nsc for creating a user account");
    }
    if (this.profile) {
      return; // Already created a user account
    }
    let newUser = new User();
    newUser.role = UserRoles.Student;
    newUser.username = this.nsc.toString();
    newUser.email = `${this.nsc.toString()}@example.com`;
    newUser.password = this.nsc.toString();
    await newUser.hashPassword();
    this.profile = newUser;
    await this.save();
  }
}
