import { User } from "@app/entity/User";
import { Ressource } from "./Ressource";
import { Category } from "./Category";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  BaseEntity,
  OneToOne,
  JoinColumn
} from "typeorm";

@Entity()
export class Post extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @OneToOne(type => User)
  @JoinColumn()
  createdBy: User;

  @Column({ length: 300, nullable: true })
  title: string;

  @Column()
  content: string;

  @Column({ default: 0 })
  votes: number;

  @ManyToMany(type => Ressource, {
    eager: true,
    nullable: true,
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinTable()
  medias: Ressource[];

  @ManyToMany(type => Category, {
    eager: true,
    nullable: true,
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinTable()
  categories: Category[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
