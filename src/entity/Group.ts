import { Student } from "./Student";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  ManyToOne,
  JoinTable,
  BaseEntity,
  JoinColumn
} from "typeorm";
import { MaxLength } from "class-validator";
import { Person } from "./Person";
import { Teacher } from "./Teacher";
@Entity()
export class Group extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  @MaxLength(300)
  name: string;

  @Column({
    nullable: true
  })
  description: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToMany(type => Student, {
    eager: true,
    nullable: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    cascade: true
  })
  @JoinTable()
  members: Student[];

  @ManyToOne(type => Teacher, {
    eager: true,
    nullable: true,
    onDelete: "NO ACTION",
    onUpdate: "CASCADE",
    cascade: true
  })
  admin: Teacher;

  @Column({ default: false })
  closed: boolean;
}
