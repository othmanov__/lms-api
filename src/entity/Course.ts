import { Discussion } from "./Discussion";
import { Unit } from "./Unit";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  BaseEntity,
  JoinColumn
} from "typeorm";
import * as bcrypt from "bcryptjs";
import { Enrolment } from "./Enrolment";

@Entity()
export class Course extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 300 })
  name: string;

  @Column({ length: 300 })
  subject: string;

  @Column({ length: 3000 })
  description: string;

  @Column()
  startAt: Date;

  @Column()
  endAt: Date;

  @OneToMany(
    type => Discussion,
    discussion => discussion.course,
    {
      nullable: true
    }
  )
  discussions: Discussion[];

  @OneToMany(
    type => Unit,
    unit => unit.course
  )
  units: Unit[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @Column({ nullable: true })
  password: string;
  @OneToMany(
    type => Enrolment,
    enrolment => enrolment.course,
    {
      nullable: true
    }
  )
  enrolments: Enrolment[];
  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  }

  checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
  }
}
