import { Person } from "./Person";
import { Column, ChildEntity } from "typeorm";
import { Matches } from "class-validator";
@ChildEntity()
export class Teacher extends Person {
  @Column({ unique: true })
  @Matches(/\d{10,20}/, {
    message: "$value is not a valid gov number"
  })
  gov_number: string;
}
