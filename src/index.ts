import { createConnection, getConnection } from "typeorm";
import "reflect-metadata";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes";
import environment from "@env";
import { Logger } from "./helpers/logger";
import { deleteFile } from "utils";
import { Server } from "http";
export type ExpressAppSettings = {
  recoveringLunch: boolean;
};
const port = environment.port;

let server:Server|any;
export function createExpressApp(
  settings?: ExpressAppSettings
): Promise<Server> {
  if (environment.type === "test") {
    if (environment.database.driver === "sqlite") {
      deleteFile(`${environment.database.name}.sqlite`);
    }
  }
  return createConnection()
    .then(async connection => {
      if (!connection.isConnected) {
        const error = new Error("Connection not set. exiting");
        Logger.Error(error);
        throw error;
      }
      if (environment.type === "test") {
        Logger.Infos("Testing Environement Detected !\r");
        Logger.Warning("Dropping schemas now for testing...\r");
        try {
          await connection.synchronize(true);
          Logger.Success("Schemas dropped successfuly");
        } catch (error) {
          Logger.Error(
            new Error(`Error Dropping Schemas! \n ${error.message}`)
          );
          throw error;
        }
      }
      Logger.Infos("Lauching server...");
      const app = express();
      const corsOptions= {
        origin:/^(http|https):\/\/localhost:\d\/*/,
        methods: ['GET', 'PUT', 'POST','DELETE','PATCH']
      }
      app.use(cors(corsOptions));
      app.use((req,res,next) => {
        Logger.Log(req,res);
        next()
      })
      app.use(helmet());
      app.use(bodyParser.json());
      app.use("/api", routes);
      server = app.listen(port, () => {
        Logger.Infos(
          `Server running on ${environment.api} in Mode: ${environment.type}`
        );
      });
      return server;
    })
    .catch(async error => {
      Logger.Error(error);
      await getConnection().close();
    });
}
if (environment.type !== "test") {
  (async function() {
    await createExpressApp();
  })();
}
export async function openConnection() {
  await getConnection().connect();
  Logger.Infos("connection opened ...");
}

export async function closeConnection() {
  await getConnection().close();
  Logger.Infos("connection closed ...");
}
