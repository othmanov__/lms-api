import { checkJwt } from "./../middlewares/checkJwt";
import { Router } from "express";
import PostController from "../controllers/PostController";

const router = Router();

router.get("/", PostController.listAll);

router.get("/:id([0-9]+)", PostController.getOneById);

router.post("/", checkJwt, PostController.newPost);

router.patch("/:id([0-9]+)", checkJwt, PostController.editPost);

router.delete("/:id([0-9]+)", checkJwt, PostController.deletePost);

export default router;
