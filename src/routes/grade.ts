import { Router } from "express";
import GradeController from "../controllers/GradeController";

const router = Router();

router.get("/", GradeController.listAll);

router.get("/:id([0-9]+)", GradeController.getOneById);

router.post("/", GradeController.newGrade);

router.patch("/:id([0-9]+)", GradeController.editGrade);

router.delete("/:id([0-9]+)", GradeController.deleteGrade);

export default router;
