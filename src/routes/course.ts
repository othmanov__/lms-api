import { Router } from "express";
import CourseController from "../controllers/CourseController";

const router = Router();

router.get("/", CourseController.listAll);

router.get("/:id([0-9]+)", CourseController.getOneById);

router.post("/", CourseController.newCourse);

router.patch("/:id([0-9]+)", CourseController.editCourse);

router.delete("/:id([0-9]+)", CourseController.deleteCourse);

export default router;
