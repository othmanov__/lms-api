import { Router } from "express";
import LessonController from "../controllers/LessonController";

const router = Router();

router.get("/", LessonController.listAll);

router.get("/:id([0-9]+)", LessonController.getOneById);

router.post("/", LessonController.newLesson);

router.patch("/:id([0-9]+)", LessonController.editLesson);

router.delete("/:id([0-9]+)", LessonController.deleteLesson);

export default router;
