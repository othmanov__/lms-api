import { Router } from "express";
import RessourceController from "../controllers/RessourceController";

const router = Router();

router.get("/", RessourceController.listAll);

router.get("/:id([0-9]+)", RessourceController.getOneById);

router.post("/", RessourceController.newRessource);

router.patch("/:id([0-9]+)", RessourceController.editRessource);

router.delete("/:id([0-9]+)", RessourceController.deleteRessource);

export default router;
