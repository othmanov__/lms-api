import { Router } from "express";
import StudentController from "../controllers/StudentController";
import { checkRole } from "../middlewares/checkRole";
import { UserRoles } from "@app/entity/User";

const router = Router();

router.get("/", [checkRole([UserRoles.Admin])], StudentController.listAll);

router.get(
  "/:id([0-9]+)",
  [checkRole([UserRoles.Admin])],
  StudentController.getOneById
);

router.post("/", [checkRole([UserRoles.Admin])], StudentController.newStudent);

router.patch(
  "/:id([0-9]+)",
  checkRole([UserRoles.Admin]),
  StudentController.editStudent
);

router.delete(
  "/:id([0-9]+)",
  checkRole([UserRoles.Admin]),
  StudentController.deleteStudent
);

export default router;
