import { Router } from "express";
import TeacherController from "../controllers/TeacherController";

const router = Router();

router.get("/", TeacherController.listAll);

router.get("/:id([0-9]+)", TeacherController.getOneById);

router.post("/", TeacherController.newTeacher);

router.patch("/:id([0-9]+)", TeacherController.editTeacher);

router.delete("/:id([0-9]+)", TeacherController.deleteTeacher);

export default router;
