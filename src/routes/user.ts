import { UserRoles } from "./../entity/User";
import { Router } from "express";
import UserController from "../controllers/UserController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

router.get(
  "/",
  [checkJwt, checkRole([UserRoles.Admin])],
  UserController.listAll
);

router.get("/:id([0-9]+)", UserController.getOneById);

router.post("/", [checkRole([UserRoles.Admin])], UserController.newUser);

router.patch(
  "/:id([0-9]+)",
  [checkRole([UserRoles.Admin])],
  UserController.editUser
);

router.delete(
  "/:id([0-9]+)",
  [checkRole([UserRoles.Admin])],
  UserController.deleteUser
);

export default router;
