import { Router } from "express";
import CategoryController from "../controllers/CategoryController";

const router = Router();

router.get("/", CategoryController.listAll);

router.get("/:id([0-9]+)", CategoryController.getOneById);

router.post("/", CategoryController.newCategory);

router.patch("/:id([0-9]+)", CategoryController.editCategory);

router.delete("/:id([0-9]+)", CategoryController.deleteCategory);

export default router;
