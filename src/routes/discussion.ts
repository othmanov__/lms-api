import { Router } from "express";
import DiscussionController from "../controllers/DiscussionController";

const router = Router();

router.get("/", DiscussionController.listAll);

router.get("/:id([0-9]+)", DiscussionController.getOneById);

router.post("/", DiscussionController.newDiscussion);

router.patch("/:id([0-9]+)", DiscussionController.editDiscussion);

router.delete("/:id([0-9]+)", DiscussionController.deleteDiscussion);

export default router;
