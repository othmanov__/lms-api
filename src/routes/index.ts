import { checkJwt } from "./../middlewares/checkJwt";
import { Router } from "express";
import auth from "./auth";
import user from "./user";
import student from "./student";
import teacher from "./teacher";
import post from "./post";
import group from "./group";
import announcement from "./announcement";
import assignment from "./assignment";
import ressource from "./ressource";
import lesson from "./lesson";
import unit from "./unit";
import course from "./course";
import enrolment from "./enrolment";
import grade from "./grade";
import discussion from "./discussion";
import category from "./category";

const routes = Router();
routes.use("/auth", auth);
routes.use("/users", checkJwt, user);
routes.use("/students", checkJwt, student);
routes.use("/teachers", checkJwt, teacher);
routes.use("/groups", checkJwt, group);
routes.use("/lessons", checkJwt, lesson);
routes.use("/enrolments", checkJwt, enrolment);
routes.use("/courses", checkJwt, course);
routes.use("/units", checkJwt, unit);
routes.use("/course/:courseId/units", checkJwt, unit);
routes.use("/grades", checkJwt, grade);
routes.use("/students/:studentId/grades", checkJwt, grade);
routes.use("/announcements", checkJwt, announcement);
routes.use("/assignments", checkJwt, assignment);
routes.use("/discussions", checkJwt, discussion);
routes.use("/posts", post);
routes.use("/cateogries", checkJwt, category);
routes.use("/uploads", checkJwt, ressource);
// fallback
routes.use("/", (req, res, next) => {
  return res.send({
    api: routes.stack
  });
});
export default routes;
