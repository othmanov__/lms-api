import { Router } from "express";
import GroupController from "../controllers/GroupController";

const router = Router();

router.get("/", GroupController.listAll);

router.get("/:id([0-9]+)", GroupController.getOneById);

router.post("/", GroupController.newGroup);

router.patch("/:id([0-9]+)", GroupController.editGroup);

router.delete("/:id([0-9]+)", GroupController.deleteGroup);

export default router;
