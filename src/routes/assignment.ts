import { Router } from "express";
import AssignmentController from "../controllers/AssignmentController";

const router = Router();

router.get("/", AssignmentController.listAll);

router.get("/:id([0-9]+)", AssignmentController.getOneById);

router.post("/", AssignmentController.newAssignment);

router.patch("/:id([0-9]+)", AssignmentController.editAssignment);

router.delete("/:id([0-9]+)", AssignmentController.deleteAssignment);

export default router;
