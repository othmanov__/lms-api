import { Router } from "express";
import AnnouncementController from "../controllers/AnnouncementController";

const router = Router();

router.get("/", AnnouncementController.listAll);

router.get("/:id([0-9]+)", AnnouncementController.getOneById);

router.post("/", AnnouncementController.newAnnouncement);

router.patch("/:id([0-9]+)", AnnouncementController.editAnnouncement);

router.delete("/:id([0-9]+)", AnnouncementController.deleteAnnouncement);

export default router;
