import { checkJwt } from "./../middlewares/checkJwt";
import { Router } from "express";
import EnrolmentController from "../controllers/EnrolmentController";

const router = Router();

router.get("/", EnrolmentController.listAll);

router.get("/:id([0-9]+)", EnrolmentController.getOneById);

router.post("/", EnrolmentController.newEnrolment);

router.patch("/:id([0-9]+)", EnrolmentController.editEnrolment);

router.delete("/:id([0-9]+)", EnrolmentController.deleteEnrolment);

export default router;
