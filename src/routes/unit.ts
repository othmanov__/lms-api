import { Router } from "express";
import UnitController from "../controllers/UnitController";

const router = Router();

router.get("/", UnitController.listAll);

router.get("/:id([0-9]+)", UnitController.getOneById);

router.post("/", UnitController.newUnit);

router.patch("/:id([0-9]+)", UnitController.editUnit);

router.delete("/:id([0-9]+)", UnitController.deleteUnit);

export default router;
