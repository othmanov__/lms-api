import * as Faker from "faker";
import { define } from "typeorm-seeding";
import { User, UserRoles } from "@app/entity/User";

define(User, (faker: typeof Faker, settings: { role: string }) => {
  const user = new User();
  user.username = "admin";
  user.password = "admin";
  user.email = "lateg15@gmail.com";
  user.isAdmin = true;
  user.role = settings.role || UserRoles.Admin;

  return user;
});
