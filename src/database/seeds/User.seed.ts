import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { User, UserRoles } from "@app/entity/User";

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const em = connection.createEntityManager();
    await em.delete(User, {});
    let user = await factory(User)({ role: UserRoles.Admin }).make();
    await user.hashPassword();
    await em.save(user);
  }
}
