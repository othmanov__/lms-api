FROM node:10

# Create app directory
RUN mkdir  /usr/src/app
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production
# Bundle app source
EXPOSE 3000
COPY . .
RUN npm test
# Build and run the app
CMD npm run prod