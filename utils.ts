import * as fs from "fs";
import * as path from "path";
import environment from "@env";
const rimraf = require("rimraf");
export async function deleteFileAsync(filename: string) {
  const file = path.resolve(filename);
  fs.exists(file, exist => {
    if (exist) {
      fs.unlink(file, err => {
        return !!err;
      });
      return true;
    }
    Promise.resolve(true);
  });
}
export function deleteFile(filename: string) {
  const file = path.resolve(filename);
  if (!fs.existsSync(file)) {
    return;
  }
  fs.unlinkSync(file);
}
export function copyFile(src: string, dest: string) {
  const src_ = path.resolve(src);
  const dest_ = path.resolve(dest);
  if (!fs.existsSync(src_)) {
    return;
  }
  fs.copyFileSync(src_, dest_);
}

export function clearMediaFolder() {
  rimraf(environment.mediaBase, () => {
    console.log("Media folder cleared ", environment.mediaBase);
  });
}
