let config = {};
require("dotenv").config();

config = {
  type: process.env.DB_DRIVER || "sqlite",
  host: process.env.DB_URL,
  database: process.env.DB_NAME,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  synchronize: true,
  logging: false,
  entities: ["src/entity/**/*.ts"],
  migrations: ["src/migration/**/*.ts"],
  subscribers: ["src/subscriber/**/*.ts"],
  seeds: ["src/database/seeds/**/*.seed.ts"],
  factories: ["src/database/factories/**/*.factory.ts"],
  cli: {
    entitiesDir: "src/entity",
    migrationsDir: "src/migration",
    subscribersDir: "src/subscriber"
  }
};

module.exports = config;
