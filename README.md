# EduPlus LMS (v2)

EduPlus is a learning management system (LMS) , it is an online platform that enables the delivery of materials, courses, resources, tools, and activities to students both in and out of the classroom environment. It allows teachers to offer tailored instructions that can be accessed by students and also to create and asses assignments related to it.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The project requirments are simple:

1. You have to setup a **database server running on your / remote computer** (if you need to use Mysql/MariaDb/Postgres/SQL Server/Mongodb )
2. An **environment variables files named '.env.prod' , '.env.local' and '.env.test'** for Production, Development and Testing respectively. Which contains all the variables for the server, database, logging , mailing and upload settings _(See the env.sample template )_.
3. Give the right permissions for _uploads_ and _logs_ folders: preferably **774**

Once complete, you need to lunch the installation ( See installation ).

### Installing

The project is not available yet on NPM, so for now you only need to clone it from the repository.

```
git clone https://bitbucket.org/othmanov__/lms_v2.git # Clone the repo
```

Once it finishes run this line to install the dependacies

    cd lms_v2 && npm install # Installs the dependacies

Then you're done if :

- You are a developper you can `npm start` to run the application on your local machine with the `env.local` environment
- You are not a developper and want to see the app running, run `npm run prod # Builds the app and run it`

## Running the tests

We use Mocha / Chai for both tests, testing have both contexts _Natural flow and Error flow_. which results of a good code coverage. and ensure the good quality of the code.
There is two types tests included in the _tests_ folder. it follows the **BDD** test style

- **End to End** tests : `npm run test-e2e`
- **Unit** tests : `npm run test-unit`

### Break down into end to end tests

The End to End (e2e) tests are for the API's endpoints. It ensures that the application is clean and working in a non technical detailed test cases. It tests the expected behavior of the application from a user perspective like:

```
it('Should a get a list of users') // Test the route of /api/users with a GET request
```

_Expected Behavior_ to return a non Empty array of Users with an HTTP status of 200.

The Unit tests are for the API's critical components. such as the ORM models operations (CRUD), the Middlewares or any other related logic like:

```
it('Should save a user in the Database') // Test the user entity wether it saves a user
```

_Expected Behavior_ to actually fetch a User after it saved to the database.

### More infos about BDD coding style.

Checkout this article about BDD.
[https://blog.testlodge.com/tdd-vs-bdd/](https://blog.testlodge.com/tdd-vs-bdd/)

## Deployment

- Using Docker _Pending_
- Using ssh / ftp / rsync :
  Simply do Prerequisites and Installation section on the target machine.

## Built With

- [ExpressJs](https://expressjs.com/) - The web framework
- [TypeORM](http://typeorm.io/) - Database ORM
- [Mocha](https://mochajs.org/) - Used to for testing
- [Nodemailer](https://nodemailer.com/) - Used for mailing

## Upcoming features

- Online discussions (Websockets) **Including Video-conferences**
- Real-time notification system (Websockets)
- File sharing/hosting (Nextcloud) integration
- Events attending managment

## Contributing

Please read [CONTRIBUTING.md](https://bitbucket.org/othmanov__/lms_v2/src/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available

## Authors

- **Othmane ElAlaoui Banouzi** - _Initial work_ - [othmanov](https://bitbucket.org/othmanov__)

See also the list of [contributors](https://bitbucket.org/othmanov__/contributors) who participated in this project.

## License

This project is licensed under the GPL v3 License - see the [LICENSE.md](LICENSE.md) file for details
