import Bool from "boolean";
import * as dotenv from "dotenv";
import * as path from "path";
import { getSeconds } from "date-fns";
dotenv.config();

let environment = {
  mailing: {
    from: process.env.FROM_ADDRESS,
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    user: process.env.SMTP_USER,
    secret: process.env.SMTP_SECRET
  },
  database: {
    driver: process.env.DB_DRIVER,
    url: process.env.DB_URL,
    name: process.env.DB_NAME,
    user: process.env.DB_USER,
    secret: process.env.DB_PASSWORD
  },
  baseDir: path.resolve(__dirname),
  host: process.env.HOSTNAME,
  port: process.env.PORT,
  mediaBase: path.resolve(process.env.MEDIA_BASE),
  salt: `${process.env.PASSWORD_SALT}`,
  logWarnings: Bool(process.env.LOG_WARNINGS),
  logErrors: Bool(process.env.LOG_ERRORS),
  logInfo: Bool(process.env.LOG_INFO),
  emails: Bool(process.env.EMAILS),
  logs: Bool(process.env.LOGS),
  logsDir: path.resolve(process.env.LOGS_LOCATION),
  // Auth:
  tokenExpiresIn: process.env.TOKEN_EXPIRES_IN
    ? Number(process.env.TOKEN_EXPIRES_IN)
    : 60 * 60 * 24,
  type: process.env.NODE_ENV,
  api: null
};
environment = {
  ...environment,
  api: `${environment.host}:${environment.port}/api`
};

export default environment;
